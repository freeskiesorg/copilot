//
//  FSDroneViewController.m
//  CoPilot
//
//  Created by Pronob Ashwin on 6/4/15.
//  Copyright (c) 2015 FreeSkies. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "FSDroneViewController.h"
#import "FSWaypointCollectionCollectionViewController.h"
#import "FSDataManager.h"
#import "FSPathModelObject.h"
#import "FSGlobalDefines.h"
#import "InspireGroundStationViewController.h"
#import "FSHelper.h"
#import "FSWebViewViewController.h"
#import "FSSaveMissionViewController.h"
#import "FSSearchViewController.h"
#import "Flurry.h"
#import "WaypointAnnotationView.h"
#import "FSWaypointAnnotation.h"
#import "FSDroneAnnotation.h"
#import "FSDroneAnnotationView.h"
#import "FSDroneMapController.h"
#import "DJIAircraftAnnotation.h"
#import "DJIAircraftAnnotationView.h"


#define ANNOTATIONID @"AnnotationID"

@interface FSDroneViewController () <MKMapViewDelegate, CLLocationManagerDelegate, UIGestureRecognizerDelegate, FSWaypointCollectionCollectionViewControllerDelegate, FSSearchViewControllerDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet MKMapView *smallMapView;

@property(nonatomic, strong) DJIAircraftAnnotation* aircraftAnnotation;

@property (nonatomic) HelpMode helpMode;

@property (weak, nonatomic) IBOutlet UIButton *map3dButton;
@property (weak, nonatomic) IBOutlet UIButton *centerLocationButton;
@property (weak, nonatomic) IBOutlet UILabel *longTextView;
@property (weak, nonatomic) IBOutlet UILabel *latTextView;
@property (weak, nonatomic) IBOutlet UIButton *zoomButton;

@property (weak, nonatomic) IBOutlet UITextField *altitudeTextField;
@property (weak, nonatomic) IBOutlet UITextField *hSpeedTextField;
@property (weak, nonatomic) IBOutlet UITextField *stayTimeTextField;
@property (weak, nonatomic) IBOutlet UILabel *droneHeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalDistanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *estimatedDistanceLabel;

@property (weak, nonatomic) IBOutlet UILabel *pitchLabel;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UILabel *heightLabel;
@property (weak, nonatomic) IBOutlet UILabel *estimatedBatteryLabel;

@property (strong, nonatomic)  MKPolyline *polyline;
@property (strong, nonatomic) MKPolylineView *lineView;

@property (weak, nonatomic) IBOutlet UILabel *totalTimeLabel;

@property (weak, nonatomic) IBOutlet UIButton *changeLocationButton;
@property (weak, nonatomic) IBOutlet UIButton *editKeyFrameButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteKeyframeButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteAllFramesButton;
@property (weak, nonatomic) IBOutlet UIButton *launchButton;

@property (nonatomic) BOOL playMode;
@property (nonatomic) BOOL isPlaying;
@property (strong, nonatomic) NSMutableArray *camerasArray;

@property (strong, nonatomic) FSWebViewViewController *fsWebViewViewController;
@property (strong, nonatomic) FSSaveMissionViewController *fsSaveMissionViewController;
@property (strong, nonatomic) FSSearchViewController *fsSearchViewController;
@property (strong, nonatomic) InspireGroundStationViewController *inspireViewController;

@property (weak, nonatomic) IBOutlet UIImageView *helpImageView;



@property (strong, nonatomic) MKUserLocation *userLocation;


@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *pauseButton;
@property (weak, nonatomic) IBOutlet UIButton *mapStateButton;

@property (nonatomic) BOOL readyForUpdate;
@property (nonatomic) BOOL editFramePressed;
@property (nonatomic) BOOL editAltitudePressed;
@property (nonatomic) BOOL editPitchAttitudePressed;


@property (strong, nonatomic) FSWaypointCollectionCollectionViewController *waypointCollectionViewController;

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (nonatomic) CLLocationDistance altitude;
@property (nonatomic) CGFloat horizontalVelocity;
@property (nonatomic) NSInteger stayTime;
@property (nonatomic) CGFloat droneAltitude;
@property (nonatomic) CGFloat pitchAttitude;
@end

@implementation FSDroneViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setPlayStatus:NO];
    
    [self setupMap];
    [self setupGestures];
    [self initializaParameters];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)setupMap
{
    // Setup Location (move to global)
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.delegate = self;
    [_locationManager startUpdatingLocation];
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager requestAlwaysAuthorization];
    
    
    //Set a few MKMapView Properties to allow pitch, building view, points of interest, and zooming.
    self.mapView.showsBuildings = YES;
    self.mapView.showsPointsOfInterest = NO;
    self.mapView.showsUserLocation = YES;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 90000
        self.mapView.mapType = MKMapTypeHybridFlyover;
        self.mapView.showsScale = YES;
        self.smallMapView.mapType = MKMapTypeHybridFlyover;
#else
        self.mapView.mapType = MKMapTypeHybrid;
#endif
    self.smallMapView.showsUserLocation = YES;
//    self.smallMapView.scrollEnabled = NO;
    self.smallMapView.rotateEnabled = NO;
//    self.smallMapView.pitchEnabled = NO;
//    self.smallMapView.zoomEnabled = NO;
    
    // Do any additional setup after loading the view from its nib.
    self.centerLocationButton.layer.borderWidth = 2.0;
    self.centerLocationButton.layer.borderColor =  [[UIColor lightGrayColor] CGColor];
    self.centerLocationButton.layer.cornerRadius = 4.0;
    
    self.map3dButton.layer.borderWidth = 2.0;
    self.map3dButton.layer.borderColor =  [[UIColor lightGrayColor] CGColor];
    self.map3dButton.layer.cornerRadius = 4.0;
    
    if ([FSDataManager sharedManager].currentPathModel != nil) {
        [self addAnnotationsFromModel:[FSDataManager sharedManager].currentPathModel];
    }
}

- (void)addAnnotationsFromModel:(FSPathModelObject *)currentPathModel
{
    for (FSWaypointModelObject *waypointModel in currentPathModel.waypointArray) {
        if (waypointModel.waypointAnnotation) {
            [self.mapView addAnnotation:waypointModel.waypointAnnotation];
            [self.smallMapView addAnnotation:waypointModel.waypointAnnotation];
        }
    }
}


- (void)setupGestures
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapAltitudeGesture:)];
    tapGesture.delegate = self;
    tapGesture.numberOfTapsRequired = 1;
    [self.droneHeightLabel addGestureRecognizer:tapGesture];
    self.droneHeightLabel.userInteractionEnabled = YES;

    UITapGestureRecognizer *tapGimbalGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGimbalGesture:)];
    tapGimbalGesture.delegate = self;
    tapGimbalGesture.numberOfTapsRequired = 1;
    [self.pitchLabel addGestureRecognizer:tapGimbalGesture];
    self.pitchLabel.userInteractionEnabled = YES;

    
    
    // Add gesture recognizer for map hoding
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    longPressGesture.delegate = self;
    longPressGesture.minimumPressDuration = 0;  // In order to detect the map touching directly (Default was 0.5)
    [self.mapView addGestureRecognizer:longPressGesture];

    UIRotationGestureRecognizer *rotationGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotationGesture:)];
    rotationGesture.delegate = self;
    [self.mapView addGestureRecognizer:rotationGesture];

    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    pinchGesture.delegate = self;
    [self.mapView addGestureRecognizer:pinchGesture];

   
    // Add gesture recognizer for map dragging
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    panGesture.delegate = self;
    [self.mapView addGestureRecognizer:panGesture];

    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipeGesture.delegate = self;
    swipeGesture.numberOfTouchesRequired = 2;
    swipeGesture.direction = UISwipeGestureRecognizerDirectionDown | UISwipeGestureRecognizerDirectionUp;
    [self.mapView addGestureRecognizer:swipeGesture];

    
    UISwipeGestureRecognizer *swipeGesture2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe2:)];
    swipeGesture2.delegate = self;
    swipeGesture2.numberOfTouchesRequired = 1;
    swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft | UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionDown | UISwipeGestureRecognizerDirectionUp;
    [self.mapView addGestureRecognizer:swipeGesture];

    
    // Add gesture recognizer for map dragging
//    UIPanGestureRecognizer *smallPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleSmallPanGesture:)];
//    smallPanGesture.delegate = self;
//    smallPanGesture.maximumNumberOfTouches = 1;  // In order to discard dragging when pinching
//    [self.smallMapView addGestureRecognizer:smallPanGesture];
}

- (void)initializaParameters
{
    self.altitude = [FSHelper getDroneAltitudeForMapCameraAltitude:self.mapView.camera.altitude]; // meters
    self.horizontalVelocity = 8.0; // m/s
    self.stayTime = 3.0; // seconds
    
//    self.altitudeTextField.text = [NSString stringWithFormat:@"%f", self.altitude];
    self.hSpeedTextField.text = [NSString stringWithFormat:@"%f", self.horizontalVelocity];
    self.stayTimeTextField.text = [NSString stringWithFormat:@"%ld", (long)self.stayTime];
    
    if ([[FSDataManager sharedManager] isFirstLaunch]) {
        [self showHelpScreen];
    } else {
        self.helpImageView.alpha = 0.0;
        [self.view sendSubviewToBack:self.helpImageView];
        self.helpMode = HELPSCREEN_DISMISSED;
    }
    
    [self resetScreenParameters];
}

- (void)resetScreenParameters
{
    self.totalDistanceLabel.text = @"TOTAL DISTANCE 0ft";
    self.estimatedBatteryLabel.text = @"EST. BATTERY 0%";
    self.totalTimeLabel.text = @"TOTAL TIME 00:00";
    
}

- (void) showHelpScreen
{
    self.helpImageView.alpha = 1.0;
    self.helpImageView.image = [UIImage imageNamed:@"Tutorial Pt1"];
    self.helpMode = HELPSCREEN_1;
    [self.view bringSubviewToFront:self.helpImageView];
    
    UITapGestureRecognizer *helpTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleHelpGesture:)];
    helpTap.delegate = self;
    [self.helpImageView addGestureRecognizer:helpTap];
}


- (void)prepareForSegue:(nonnull UIStoryboardSegue *)segue sender:(nullable id)sender
{
    if ([segue.identifier isEqualToString:@"waypointEmbedSegue"]) {
        self.waypointCollectionViewController = segue.destinationViewController;
        self.waypointCollectionViewController.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"showWeatherSegue"]) {
        self.fsWebViewViewController = segue.destinationViewController;
        self.fsWebViewViewController.webURLString = @"http://www.weather.com/weather/today/l/37.45,-122.18?lat=37.45&lon=-122.18&locale=en_US&temp=f";
    }
    
    if ([segue.identifier isEqualToString:@"showNoFlyZonesSegue"]) {
        self.fsWebViewViewController = segue.destinationViewController;
        self.fsWebViewViewController.webURLString = @"https://www.mapbox.com/drone/no-fly/";
    }
    
    if ([segue.identifier isEqualToString:@"saveMissionSegue"]) {
        self.fsSaveMissionViewController = segue.destinationViewController;
        [self.fsSaveMissionViewController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    }
    
    if ([segue.identifier isEqualToString:@"locateSegue"]) {
        self.fsSearchViewController = segue.destinationViewController;
        self.fsSearchViewController.delegate = self;
        [self.fsSearchViewController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    }
    
    
    if ([segue.identifier isEqualToString:@"launchMissionSegue"]) {
        self.inspireViewController = segue.destinationViewController;
    }
}


- (void)snapShotMapData
{
    WeakSelf weakSelf = self;
    
    [self createMapSnapshotWithImageSize:CGSizeMake(300, 300) forMapType:MKMapTypeStandard completion:^(NSData *imageData) {
        if (weakSelf) {
            StrongSelf strongSelf = weakSelf;
            if(strongSelf) {
                [FSDataManager sharedManager].currentPathModel.standardMapImageData = imageData;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (strongSelf.fsSaveMissionViewController) {
                        [strongSelf.fsSaveMissionViewController refreshStandardImage:imageData];
                    }
                });
            }
        }
    }];
    
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 90000
    MKMapType maptype = MKMapTypeHybridFlyover;
#else
    MKMapType maptype = MKMapTypeHybrid;
#endif
    
    [self createMapSnapshotWithImageSize:CGSizeMake(300, 300) forMapType:maptype completion:^(NSData *imageData) {
        if (weakSelf) {
            StrongSelf strongSelf = weakSelf;
            if(strongSelf) {
                [FSDataManager sharedManager].currentPathModel.hybridMapImageData = imageData;
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (strongSelf.fsSaveMissionViewController) {
                        [strongSelf.fsSaveMissionViewController refreshHybridImage:imageData];
                    }
                });
            }
        }
    }];
}

// Allow to recognize multiple gestures simultaneously (Implementation of the protocole UIGestureRecognizerDelegate)

- (void)handleHelpGesture:(UIGestureRecognizer *)sender {
    if (self.helpMode == HELPSCREEN_1) {
        self.helpImageView.image = [UIImage imageNamed:@"Tutorial Pt2"];
        self.helpMode = HELPSCREEN_2;
        return;
    }
    
    self.helpMode = HELPSCREEN_DISMISSED;
    self.helpImageView.alpha = 0.0;
    
    [self performSegueWithIdentifier:@"locateSegue" sender:self];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)handlePinchGesture:(UIGestureRecognizer *)sender {
//    [self syncMaps];
    self.editFramePressed = NO;
}

- (void)handleRotationGesture:(UIGestureRecognizer *)sender {
    UIRotationGestureRecognizer *rotationGuesture = (UIRotationGestureRecognizer *)sender;

    [self showDroneCoordinateOnSmallMap:[self getDroneCoordinates]];
//    [self syncMaps];
    self.editFramePressed = NO;
}

- (void)handleTapGimbalGesture:(UIGestureRecognizer *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"New Pitch Angle" message:@"Enter a new pitch angle (-90° to 30°)" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * __nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"Enter Angle in degrees", @"Enter Angle in degrees");
        textField.keyboardType = UIKeyboardTypeNumberPad;
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * __nonnull action) {
        UITextField *textField = alertController.textFields.firstObject;
        
        if ([FSHelper isNumeric:textField.text]) {
            double dAttitude = [textField.text doubleValue];
            if ((dAttitude >= -90.0)&&(dAttitude <= 30.00)) {
                self.pitchAttitude = dAttitude;
                self.pitchLabel.text = [NSString stringWithFormat:@"PITCH %@°", textField.text];
                [self.pitchLabel setTextColor:[UIColor colorWithRed:0.29 green:0.56 blue:0.88 alpha:1.0]];
                
//                MKMapCamera *camera = [self.mapView.camera copy];
//                camera.pitch = [FSHelper convertToCameraAngle:self.pitchAttitude];
//                
//                [self.mapView setCamera:camera animated:YES];
                self.editPitchAttitudePressed = YES;
                
                if (self.editKeyFrameButton.enabled) {
                    [self performSelector:@selector(updateKeyframe:) withObject:nil afterDelay:0.2];
                }
            }
        }
    }];
    
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)handleTapAltitudeGesture:(UIGestureRecognizer *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"New Altitude" message:@"Enter a new altitude for this waypoint." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * __nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"Enter Altitude in Feet", @"Enter Altitude in Feet");
        textField.keyboardType = UIKeyboardTypeNumberPad;
    }];

    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * __nonnull action) {
        UITextField *textField = alertController.textFields.firstObject;
        
        if ([FSHelper isNumeric:textField.text]) {
            double dAltitude = [textField.text doubleValue];
            if ((dAltitude > 3.0)&&(dAltitude < 1000.00)) {
                self.droneAltitude = dAltitude;
                self.droneHeightLabel.text = [NSString stringWithFormat:@"MOD DRONE HT %@ ft", textField.text];
                [self.droneHeightLabel setTextColor:[UIColor colorWithRed:0.29 green:0.56 blue:0.88 alpha:1.0]];

                CGFloat altitudeInMeters = self.droneAltitude * FEET_TO_METERS;
                MKMapCamera *camera = [self.mapView.camera copy];
                camera.altitude = altitudeInMeters;
                
                [self.mapView setCamera:camera animated:YES];
                self.editAltitudePressed = YES;
                
                if (self.editKeyFrameButton.enabled) {
                    [self performSelector:@selector(updateKeyframe:) withObject:nil afterDelay:0.2];
                }
            }
        }
    }];
    

    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

// On map holding or pinching pause localise and heading
- (void)handleLongPressGesture:(UIGestureRecognizer *)sender {
    //L360LogVerbose(@"Pinch");
    [self showDroneCoordinateOnSmallMap:[self getDroneCoordinates]];
}

// On dragging gesture put map in free mode
- (void)handlePanGesture:(UIGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded){
    }
    
    [self syncMaps];
    self.editFramePressed = NO;
}


- (void)handleSmallPanGesture:(UIGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateBegan){
        self.readyForUpdate = YES;
    }
    if (sender.state == UIGestureRecognizerStateEnded){
    }

    
    CLLocationDistance droneAltitude =[FSHelper getDroneAltitudeForMapCameraAltitude:self.mapView.camera.altitude];
    CGFloat dronePitch = self.mapView.camera.pitch;
    CLLocationDirection droneHeading = self.mapView.camera.heading;
    CLLocationCoordinate2D droneCoordinate = self.smallMapView.centerCoordinate;
    
    CLLocationCoordinate2D mapCoordinate = [FSHelper getMapCameraCoordinateForDroneAltitude:droneAltitude pitch:dronePitch withBearing:droneHeading andDroneCoordinate:droneCoordinate];

    self.mapView.centerCoordinate = mapCoordinate;
}

- (void)handleSwipe:(UIGestureRecognizer *)sender {
    self.editFramePressed = NO;
    [self syncMaps];
}

- (void)handleSwipe2:(UIGestureRecognizer *)sender {
    [self syncMaps];
}

- (void)rotateDroneImageView
{
    float degrees = self.mapView.camera.heading; //the value in degrees
    double heading = RADIAN(degrees);
    DJIAircraftAnnotationView* annoView = (DJIAircraftAnnotationView*)[self.smallMapView viewForAnnotation:self.aircraftAnnotation];
    [annoView updateHeading:heading];
}

- (void)syncMaps
{
    [self rotateDroneImageView];
    [self.mapView setUserTrackingMode:MKUserTrackingModeNone];
    [self.smallMapView setUserTrackingMode:MKUserTrackingModeNone];
    
    
    [self setCameraLabels:self.mapView.camera];
    [self showDroneCoordinateOnSmallMap:[self getDroneCoordinates]];
}


- (CLLocationCoordinate2D)getDroneCoordinates
{
    CLLocationCoordinate2D droneCoordinates = [FSHelper getDroneCoordinateForMapCamera:self.mapView.camera lookingAtCenterCoordinate:self.mapView.centerCoordinate];
    return droneCoordinates;
}

- (void)showDroneCoordinateOnSmallMap:(CLLocationCoordinate2D) droneCoordinates
{
    MKCoordinateRegion region = {0};
    
    if (self.zoomButton.selected) {
        region.span = MKCoordinateSpanMake(0.00025, 0.00040);
    } else {
        region.span = MKCoordinateSpanMake(0.0025, 0.0040);
    }
    
    region.center = droneCoordinates;

    if (self.aircraftAnnotation == nil) {
        self.aircraftAnnotation = [[DJIAircraftAnnotation alloc] initWithCoordiante:droneCoordinates];
        [self.smallMapView addAnnotation:self.aircraftAnnotation];
    }
    
    [self.aircraftAnnotation setCoordinate:droneCoordinates];
    
    [self rotateDroneImageView];
    self.smallMapView.region = region;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.mapView setUserTrackingMode: MKUserTrackingModeFollow animated: YES];
    [self.smallMapView setUserTrackingMode: MKUserTrackingModeFollow animated: YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark User Heading
- (void)syncMapViewLocations
{
    [self setUserTrackingForMapView:self.mapView];
    [self setUserTrackingForMapView:self.smallMapView];
}

- (void)setUserTrackingForMapView:(MKMapView *)mapView
{
    if(mapView.userTrackingMode != MKUserTrackingModeFollow)
        [mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    else
        [mapView setUserTrackingMode:MKUserTrackingModeNone animated:YES];
}

#pragma mark MKMapViewDelegate


// Location Manager Delegate Methods
- (void)mapView:(nonnull MKMapView *)mapView didUpdateUserLocation:(nonnull MKUserLocation *)userLocation
{
    self.userLocation = userLocation;
}


// Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id < MKAnnotation >)annotation
{
    if([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }

    if ([annotation isKindOfClass:[FSWaypointAnnotation class]])
    {
        FSWaypointAnnotation *waypointAnnotation  = (FSWaypointAnnotation *)annotation;
        WaypointAnnotationView *pinView = (WaypointAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:ANNOTATIONID];
        
        if (!pinView) {
            pinView = [[WaypointAnnotationView alloc] initWithAnnotation:waypointAnnotation reuseIdentifier:ANNOTATIONID];
        }
        

        [self configureAnnotationView:pinView viewForAnnotation:waypointAnnotation];
        return pinView;
    }
    else if ([annotation isKindOfClass:[DJIAircraftAnnotation class]])
    {
        static NSString* aircraftReuseIdentifier = @"DJI_AIRCRAFT_ANNOTATION_VIEW";
        DJIAircraftAnnotationView* aircraftAnno = (DJIAircraftAnnotationView*)[self.smallMapView dequeueReusableAnnotationViewWithIdentifier:aircraftReuseIdentifier];
        if (aircraftAnno == nil) {
            aircraftAnno = [[DJIAircraftAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:aircraftReuseIdentifier];
        }
        return aircraftAnno;
    }
    return nil;
}

- (void)configureAnnotationView:(MKAnnotationView *)annotationView viewForAnnotation:(id < MKAnnotation >)annotation
{
    FSWaypointAnnotation *waypointAnnotation = (FSWaypointAnnotation *)annotation;
    
    UILabel *labelView = (UILabel *)[annotationView viewWithTag:1];
    
    if (labelView == nil)
    {
        //create and add label...
        labelView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        labelView.textAlignment = NSTextAlignmentCenter;
        labelView.tag = 1;
        labelView.textColor = [UIColor whiteColor];
        [annotationView addSubview:labelView];
    }
    
    labelView.text = waypointAnnotation.waypointLabel;
}


- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    for (NSObject *annotation in [mapView annotations])
    {
        if ([annotation isKindOfClass:[DJIAircraftAnnotation class]])
        {
            MKAnnotationView *aircraftAnnotationView = [mapView viewForAnnotation:(DJIAircraftAnnotation *)annotation];
            [[aircraftAnnotationView superview] bringSubviewToFront:aircraftAnnotationView];
        }
    }
    
    if (self.editKeyFrameButton.enabled) {
        if (!self.editFramePressed) {
            [self.editKeyFrameButton setImage:[UIImage imageNamed:@"Done Button"] forState:UIControlStateNormal];
        } else {
            [self.editKeyFrameButton setImage:[UIImage imageNamed:@"Edit Keyframe Button"] forState:UIControlStateNormal];
        }
    }
    
    if (self.isPlaying) {
        if (!self.playMode) {
            self.isPlaying = NO;
            return;
        }
        self.isPlaying = NO;
        NSLog(@"%d waypoint reached", [self.camerasArray count]);
        [self performSelector:@selector(gotoNextCamera) withObject:nil afterDelay:3.0];
    }
    
    if (self.mapView == mapView) {
//        NSLog(@"MAPX Lat: %f Long: %f Altitude: %f Pitch: %f", mapView.camera.centerCoordinate.latitude, mapView.camera.centerCoordinate.longitude, mapView.camera.altitude, mapView.camera.pitch);
        if ((!self.editAltitudePressed)&&(!self.editPitchAttitudePressed)) {
            [self setCameraLabels:self.mapView.camera];
        }
        
        CLLocationCoordinate2D centre = [self.mapView centerCoordinate];
        
        self.latTextView.text = [NSString stringWithFormat:@"LATITUDE  %f", centre.latitude];
        self.longTextView.text = [NSString stringWithFormat:@"LONGITUDE  %f", centre.longitude];
        
        if(self.mapView.userTrackingMode == MKUserTrackingModeFollow)
        {
            [self.centerLocationButton setImage:[UIImage imageNamed:@"LocationIconDark"] forState:UIControlStateNormal];
            self.centerLocationButton.backgroundColor = [UIColor colorWithRed:0.5 green:0.5  blue:0.5  alpha:0.7];
        } else
        {
            [self.centerLocationButton setImage:[UIImage imageNamed:@"LocationIcon"] forState:UIControlStateNormal];
            self.centerLocationButton.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.2];
        }
        
        if (!self.readyForUpdate) {
            if ((self.mapView.userTrackingMode == MKUserTrackingModeNone)&&([self.mapView.userLocation isUpdating])) {

                if (!CLCOORDINATES_EQUAL(self.smallMapView.centerCoordinate, [self getDroneCoordinates])) {
                    [self showDroneCoordinateOnSmallMap:[self getDroneCoordinates]];
                }
            }
        }

    }
    
    if (self.smallMapView == mapView) {
        if (self.readyForUpdate) {
            if ((self.smallMapView.userTrackingMode == MKUserTrackingModeNone)&&([self.smallMapView.userLocation isUpdating])) {

                CLLocationCoordinate2D mapCoordinate = [FSHelper getMapCameraCoordinateForDroneAltitude:self.mapView.camera.altitude*DRONE_CAMERA_COMPENSATION_FACTOR pitch:self.mapView.camera.pitch withBearing:self.mapView.camera.heading andDroneCoordinate:self.smallMapView.centerCoordinate];

                if (!CLCOORDINATES_EQUAL(self.mapView.centerCoordinate, mapCoordinate)) {
//                    self.mapView.centerCoordinate = mapCoordinate;
                }
            }
        }
        self.readyForUpdate = NO;
    }
}


- (void) mapView:(MKMapView *)aMapView didAddAnnotationViews:(NSArray *)views
{
    for (MKAnnotationView *view in views)
    {
        if ([[view annotation] isKindOfClass:[DJIAircraftAnnotation class]])
        {
            [[view superview] bringSubviewToFront:view];
        }
        else
        {
            [[view superview] sendSubviewToBack:view];
        }
    }
}

#pragma mark SnapShot

- (NSString *)createSnapshotFilename
{
    return [self.waypointCollectionViewController createWaypointName];
}

- (NSString *)createSnapshotPathName
{
    NSString *pathName = [NSString stringWithFormat:@"path%lu", (unsigned long)[[FSDataManager sharedManager].pathsArray count]];

    return pathName;
}



- (void)createMapSnapshotWithImageSize:(CGSize)size forMapType:(MKMapType)mapType completion:(ImageDataCompletionBlock)onCompletion
{
    MKMapSnapshotOptions *snapshotOptions = [MKMapSnapshotOptions new];
    snapshotOptions.region = [[FSDataManager sharedManager].currentPathModel getSpanRegionForWaypoints];
    snapshotOptions.size = size;
    snapshotOptions.mapType = mapType;
    
    MKMapSnapshotter *shotter =[[MKMapSnapshotter alloc] initWithOptions:snapshotOptions];
    [shotter startWithQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) completionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
        // get the image associated with the snapshot
        
        UIImage *image = snapshot.image;
        // Get the size of the final image
        
        CGRect finalImageRect = CGRectMake(0, 0, image.size.width, image.size.height);
        
        // Get a standard annotation view pin. Clearly, Apple assumes that we'll only want to draw standard annotation pins!
        
        WaypointAnnotationView *pinView = [[WaypointAnnotationView alloc] initWithAnnotation:nil reuseIdentifier:ANNOTATIONID];
        UIImage *pinImage = pinView.image;
        
        // ok, let's start to create our final image
        
        UIGraphicsBeginImageContextWithOptions(image.size, YES, image.scale);
        
        // first, draw the image from the snapshotter
        
        [image drawAtPoint:CGPointMake(0, 0)];
        
        // now, let's iterate through the annotations and draw them, too
        
        for (id<MKAnnotation>annotation in self.mapView.annotations)
        {
            CGPoint point = [snapshot pointForCoordinate:annotation.coordinate];
            if (CGRectContainsPoint(finalImageRect, point)) // this is too conservative, but you get the idea
            {
                if(![annotation isKindOfClass:[MKUserLocation class]]) {
                    CGPoint pinCenterOffset = pinView.centerOffset;
                    point.x -= pinView.bounds.size.width / 2.0;
                    point.y -= pinView.bounds.size.height / 2.0;
                    point.x += pinCenterOffset.x;
                    point.y += pinCenterOffset.y;
                    
                    [pinImage drawAtPoint:point];
                
                    FSWaypointAnnotation *waypointAnnotation = (FSWaypointAnnotation *)annotation;
                    
                    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
                    paragraphStyle.alignment = NSTextAlignmentCenter;
                    UIFont *textFont = [UIFont systemFontOfSize:16];
                    UIColor *whiteColor = [UIColor whiteColor];
                    
                    NSDictionary *dictionary = @{ NSFontAttributeName: textFont,
                                                  NSParagraphStyleAttributeName: paragraphStyle,
                                                  NSForegroundColorAttributeName: whiteColor};
                    
                    point.x += pinView.bounds.size.width / 3.0;
                    point.y += 4;
                    
                    [waypointAnnotation.waypointLabel drawAtPoint:point withAttributes:dictionary];
                }
            }
        }
        
        // grab the final image
        
        UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        // and save it
        
        NSData *data = UIImagePNGRepresentation(finalImage);
        [data writeToFile:[self createSnapshotPathName] atomically:YES];
        if (onCompletion) {
            onCompletion(data);
        }

        
            if (onCompletion) {
                onCompletion(data);
            }
    }];
}

- (void) snapShotCurrentMapWithCompletion:(ImageDataCompletionBlock)onCompletion
{
    MKMapSnapshotOptions *options = [[MKMapSnapshotOptions alloc] init];
    options.size = CGSizeMake(160, 125);
    options.scale = [[UIScreen mainScreen] scale];
    options.camera = self.mapView.camera;
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 90000
    options.mapType = MKMapTypeHybridFlyover;
#else
    options.mapType = MKMapTypeHybrid;
#endif
    
    
    MKMapSnapshotter *snapshotter = [[MKMapSnapshotter alloc] initWithOptions:options];
    [snapshotter startWithQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) completionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
        
        // get the image associated with the snapshot
        
        UIImage *image = snapshot.image;
        NSData *data = UIImagePNGRepresentation(image);
        [data writeToFile:[self createSnapshotFilename] atomically:YES];
        
        if (onCompletion) {
            onCompletion(data);
        }
        

    }];
}


#pragma mark waypoint delegate helpers

- (void)addKeyframeWithCompletion:(DelegateCompletionBlock)completion
{
    CLLocationDistance droneAltitude = [FSHelper getDroneAltitudeForMapCameraAltitude:self.mapView.camera.altitude];

    __block FSDataManager *fsDataManager = [FSDataManager sharedManager];
    __block FSPathModelObject* currentPathModel = fsDataManager.currentPathModel;
    
    //    self.altitude = [FSHelper getDroneAltitudeForMapCameraAltitude:self.mapView.camera.altitude];
    //    self.stayTime = [self.stayTimeTextField.text integerValue];
    //    self.horizontalVelocity = [self.hSpeedTextField.text integerValue];
    
    
    CLLocationCoordinate2D droneCoordinates = [self getDroneCoordinates];
    
    // Place a single pin
    __block FSWaypointAnnotation *annotation = [[FSWaypointAnnotation alloc] initWithCoordinate:droneCoordinates andWaypointLabel:@""];
    [annotation setCoordinate:droneCoordinates];
    
    // Flurry
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [NSString stringWithFormat:@"%f", droneCoordinates.latitude], @"DroneCoordinatesLat",
                                   [NSString stringWithFormat:@"%f", droneCoordinates.longitude], @"DroneCoordinatesLong",
                                   [NSString stringWithFormat:@"%f", droneAltitude], @"DroneAltitude",
                                   [NSString stringWithFormat:@"%f", self.mapView.camera.heading], @"DroneHeading",nil];
    
    [Flurry logEvent:@"Add Waypoint" withParameters:articleParams];
    
    WeakSelf weakSelf = self;
    __block MKMapCamera *camera = [self.mapView.camera copy];
    __block CLLocationCoordinate2D centerCoordinate = self.mapView.centerCoordinate;
    NSLog(@"Before Lat: %f Long: %f DroneLat: %f DroneLong: %f Altitude: %f Pitch: %f", camera.centerCoordinate.latitude, camera.centerCoordinate.longitude, droneCoordinates.latitude, droneCoordinates.longitude, camera.altitude, camera.pitch);

    [self snapShotCurrentMapWithCompletion:^(NSData *imageData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            StrongSelf strongSelf = weakSelf;
            if(strongSelf) {

                CGFloat droneAltitudeInMeters = [FSHelper getDroneAltitudeForMapCameraAltitude:camera.altitude];
                CGFloat dronePitch = [FSHelper convertToDJIAngle:camera.pitch];

                
                if ([strongSelf.droneHeightLabel.textColor isEqual:[UIColor colorWithRed:0.29 green:0.56 blue:0.88 alpha:1.0]]) {
                    if (strongSelf.editAltitudePressed) {
                        droneAltitudeInMeters = strongSelf.droneAltitude * FEET_TO_METERS;
                    }
                }
                
                if ([strongSelf.pitchLabel.textColor isEqual:[UIColor colorWithRed:0.29 green:0.56 blue:0.88 alpha:1.0]]) {
                    if (strongSelf.editPitchAttitudePressed) {
                        dronePitch = strongSelf.pitchAttitude;
                    }
                }

                NSLog(@"Saved Lat: %f Long: %f DroneLat: %f DroneLong: %f Altitude: %f Pitch: %f", camera.centerCoordinate.latitude, camera.centerCoordinate.longitude, droneCoordinates.latitude, droneCoordinates.longitude, camera.altitude, camera.pitch);
                [currentPathModel addWaypointLocationForMapCamera:camera droneAltitude:droneAltitudeInMeters dronePitch:dronePitch withHorizontalVelocity:strongSelf.horizontalVelocity stayTime:strongSelf.stayTime lookingAtCoordinate:centerCoordinate forMapImageData:imageData andAnnotation:annotation];
                [annotation setTitle:[NSString stringWithFormat:@"Altitude: %0.2f ft; Pitch: %0.2f°", strongSelf.droneAltitude, strongSelf.pitchAttitude]];
                
                [strongSelf.mapView addAnnotation:annotation];
                [strongSelf.smallMapView addAnnotation:annotation];
                
                //                [strongSelf drawLines];
                NSDictionary *dictionary = [FSHelper getEstimatedTimeForWaypointIndex:([currentPathModel.waypointArray count]-1)];
                NSNumber *estimatedTime = [dictionary objectForKey:@"estimatedTime"];
                NSNumber *estimatedBattery = [dictionary objectForKey:@"estimatedBatteryUsage"];
                NSNumber *totalDistance = [dictionary objectForKey:@"totalDistance"];
                
                NSString *estimatedTimeString = [FSHelper convertTimeToString:[estimatedTime floatValue]];
                
                
                strongSelf.estimatedBatteryLabel.text = [NSString stringWithFormat:@"EST. BATTERY %0.2f%%", [estimatedBattery floatValue]];
                strongSelf.totalTimeLabel.text = [NSString stringWithFormat:@"TOTAL TIME %@", estimatedTimeString];
                self.totalDistanceLabel.text = [NSString stringWithFormat:@"TOTAL DISTANCE %0.2fft", ([totalDistance floatValue] * METERS_TO_FEET) ];

            
                strongSelf.editFramePressed = NO;
                strongSelf.editAltitudePressed = NO;
                strongSelf.editPitchAttitudePressed = NO;
                
                strongSelf.editKeyFrameButton.enabled = NO;
                strongSelf.deleteKeyframeButton.enabled = NO;
                
                strongSelf.deleteAllFramesButton.enabled = YES;
                

            }
        });
        if (completion) {
            completion(YES);
        }
    }];
}

#pragma mark draw lines

- (void)drawLines {
    
    // remove polyline if one exists
    [self.mapView removeOverlay:self.polyline];
    [self.smallMapView removeOverlay:self.polyline];
    
    // create an array of coordinates from allPins
    CLLocationCoordinate2D coordinates[[FSDataManager sharedManager].currentPathModel.waypointArray.count];
    
    int i = 0;

    for (FSWaypointModelObject *waypoint in [FSDataManager sharedManager].currentPathModel.waypointArray) {
        coordinates[i] = waypoint.waypoint.coordinate;
        i++;
    }
    
    // create a polyline with all cooridnates
    MKPolyline *polyline = [MKPolyline polylineWithCoordinates:coordinates count:[FSDataManager sharedManager].currentPathModel.waypointArray.count];
    [self.mapView addOverlay:polyline];
    [self.smallMapView addOverlay:polyline];
    
    self.polyline = polyline;
    
    // create an MKPolylineView and add it to the map view
    self.lineView = [[MKPolylineView alloc]initWithPolyline:self.polyline];
    self.lineView.strokeColor = [UIColor redColor];
    self.lineView.lineWidth = 2;
    
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay {
    
    return self.lineView;
}

#pragma mark FSWaypointCollectionCollectionViewControllerDelegate

- (void)clearSelectionForWaypointCollectionViewController:(id)waypointCollectionViewController
{
    self.mapView.layer.borderColor = [UIColor clearColor].CGColor;
    self.mapView.layer.borderWidth = 1.0;
    
    [self.editKeyFrameButton setImage:[UIImage imageNamed:@"Edit Keyframe Button"] forState:UIControlStateNormal];
    self.deleteKeyframeButton.enabled = NO;
    self.editKeyFrameButton.enabled = NO;
    self.deleteAllFramesButton.enabled = NO;
    
    self.editFramePressed = NO;
    self.editAltitudePressed = NO;
    self.editPitchAttitudePressed = NO;
    
    [self.waypointCollectionViewController resetCollectionView];
    
}

- (void)addNewWaypointToCollectionViewController:(id)waypointCollectionViewController withCompletion:(DelegateCompletionBlock)completion
{
    self.mapView.layer.borderColor = [UIColor clearColor].CGColor;
    self.mapView.layer.borderWidth = 1.0;
    
    if (![self.mapView.userLocation isUpdating]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Map Updating." message:@"Please wait..." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alertController animated:YES completion:^{
                if(completion) {
                    completion(NO);
                }
            }];
        });
        
        if(completion) {
            completion(NO);
        }
        return;
    }
    
    CGFloat currentDroneAltitude;
    if (self.editAltitudePressed) {
        currentDroneAltitude = self.droneAltitude * FEET_TO_METERS;
    } else {
        currentDroneAltitude = [FSHelper getDroneAltitudeForMapCameraAltitude:self.mapView.camera.altitude];
    }
    if (currentDroneAltitude > 121) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Your Waypoint altitude is too high. Would you like to continue to Add Keyframe?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"YES", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self addKeyframeWithCompletion:completion];
                                   }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction * __nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alertController animated:YES completion:^{
                if(completion) {
                    completion(YES);
                }
            }];
        });
        return;
    }
    
    [self addKeyframeWithCompletion:completion];
}

- (void)waypointCollectionViewController:(id)waypointCollectionViewController updateMapWithWaypointData:(FSWaypointModelObject *)waypointModelObject {
    MKMapCamera *nextCam1 = [MKMapCamera cameraLookingAtCenterCoordinate:waypointModelObject.lookingAtCoordinate fromEyeCoordinate:[FSHelper getCameraCoordinateForCameraAltitude:waypointModelObject.mapCamera.altitude lookingAtCenterCoordinate:waypointModelObject.mapCamera.centerCoordinate forMapCamera:waypointModelObject.mapCamera] eyeAltitude:waypointModelObject.mapCamera.altitude];
    MKMapCamera *nextCam2 = [MKMapCamera cameraLookingAtCenterCoordinate:waypointModelObject.lookingAtCoordinate fromEyeCoordinate:[FSHelper getCameraCoordinateForCameraAltitude:waypointModelObject.waypoint.altitude*1.4 lookingAtCenterCoordinate:waypointModelObject.mapCamera.centerCoordinate forMapCamera:waypointModelObject.mapCamera] eyeAltitude:waypointModelObject.waypoint.altitude*1.4];

    MKMapCamera *nextCam = [waypointModelObject.mapCamera copy];

    self.mapView.layer.borderColor = [UIColor colorWithRed:0.29 green:0.56 blue:0.88 alpha:1.0].CGColor;
    self.mapView.layer.borderWidth = 2.0;

    [UIView animateWithDuration:UPDATE_ANIMATION_SPEED animations:^{
        self.mapView.camera = nextCam;
    }];
    
    self.altitude = waypointModelObject.waypoint.altitude;
//    self.stayTime = waypointModelObject.waypoint.stayTime;
//    self.horizontalVelocity = waypointModelObject.waypoint.horizontalVelocity;
    
    self.droneAltitude = [FSHelper toFeet:waypointModelObject.waypoint.altitude];
    self.pitchAttitude = [waypointModelObject getPitchAction];
    
    self.droneHeightLabel.text = [NSString stringWithFormat:@"DRONE HT %0.2f ft", self.droneAltitude];
    if (self.droneAltitude > 400) {
        [self.droneHeightLabel setBackgroundColor:[UIColor redColor]];
    } else {
        [self.droneHeightLabel setBackgroundColor:[UIColor clearColor]];
    }

    [self.droneHeightLabel setTextColor:[UIColor whiteColor]];
    [self.pitchLabel setTextColor:[UIColor whiteColor]];
    self.headingLabel.text = [NSString stringWithFormat:@"HEADING %0.2f°", waypointModelObject.waypoint.heading];
    self.pitchLabel.text = [NSString stringWithFormat:@"PITCH %0.2f°", waypointModelObject.cameraPitch];
    
    self.editKeyFrameButton.enabled = YES;
    self.editFramePressed = YES;
    self.editAltitudePressed = YES;
    self.editPitchAttitudePressed = YES;
    self.deleteKeyframeButton.enabled = YES;
    self.deleteAllFramesButton.enabled = YES;
}


- (void)setCameraLabels:(MKMapCamera *)camera {
    self.droneAltitude = [FSHelper getDroneAltitudeForMapCameraAltitude:[FSHelper toFeet:self.mapView.camera.altitude]];
    self.droneHeightLabel.text = [NSString stringWithFormat:@"DRONE HT %0.2f ft", self.droneAltitude];
//    NSLog(@"Map Camera Latitude: %f, Longitude: %f, Altitude: %f, Pitch: %f", camera.centerCoordinate.latitude, camera.centerCoordinate.longitude, camera.altitude, camera.pitch);
    
    [self.droneHeightLabel setTextColor:[UIColor whiteColor]];
    [self.pitchLabel setTextColor:[UIColor whiteColor]];
    self.editAltitudePressed = NO;
    self.editPitchAttitudePressed = NO;
    

    if (self.mapView.camera.altitude > ALTITUDE_CAP) {
        [self.droneHeightLabel setBackgroundColor:[UIColor redColor]];
    } else {
        [self.droneHeightLabel setBackgroundColor:[UIColor clearColor]];
    }
    
    self.headingLabel.text = [NSString stringWithFormat:@"HEADING %0.2f°", camera.heading];
    self.pitchLabel.text = [NSString stringWithFormat:@"PITCH %0.2f°", [FSHelper convertToDJIAngle:camera.pitch]];
}

#pragma mark reset and zoom map

- (void) zoomSmallMap
{
    MKCoordinateRegion region;
    region.center = self.smallMapView.centerCoordinate;
    region.span = MKCoordinateSpanMake(0.00025, 0.00040);
    [self.smallMapView setRegion:region animated:YES];
    self.readyForUpdate = YES;
    
    [self.smallMapView setUserTrackingMode:MKUserTrackingModeNone];
}

- (void) resetSmallMap
{
    MKCoordinateRegion region;
    region.center = self.smallMapView.centerCoordinate;
    region.span = MKCoordinateSpanMake(0.0025, 0.0040);
    [self.smallMapView setRegion:region animated:YES];
    self.readyForUpdate = YES;
    
    [self.smallMapView setUserTrackingMode:MKUserTrackingModeNone];
}


#pragma mark Map PlayBack


-(void)flyToLocation:(FSWaypointModelObject *)model {
    
    FSDataManager *fsDataManager = [FSDataManager sharedManager];
    FSPathModelObject* currentPathModel = fsDataManager.currentPathModel;
    self.camerasArray = [NSMutableArray arrayWithArray:currentPathModel.waypointArray];

    if (model != nil) {
        NSInteger index = [currentPathModel.waypointArray indexOfObject:model];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, index)];
        [self.camerasArray removeObjectsAtIndexes:indexSet];
    }
    
    self.playMode = YES;
    
    [self gotoNextCamera];
    
}

- (void)setPlayStatus:(BOOL)on
{
    if (on) {
        self.isPlaying = on;
        self.playMode = on;
        self.playButton.alpha = 0.0;
        self.pauseButton.alpha = 1.0;
        return;
    }
    self.isPlaying = on;
    self.playMode = on;
    self.playButton.alpha = 1.0;
    self.pauseButton.alpha = 0.0;
    
}


-(void)gotoNextCamera {
    if (!self.playMode) {
        self.isPlaying = NO;
        return;
    }
    if (self.camerasArray.count == 0) {
        [self setPlayStatus:NO];
        return;
    }
    
    self.isPlaying = YES;
    
    FSWaypointModelObject *waypointObject = [self.camerasArray firstObject];
    MKMapCamera *nextCam = waypointObject.mapCamera;
    [self.camerasArray removeObjectAtIndex:0];
    
    [UIView animateWithDuration:PLAYBACK_ANIMATION_SPEED animations:^{
        self.mapView.camera = nextCam;
    }];
    
}

#pragma mark IBActions

- (IBAction)show3DViewButtonClicked:(id)sender {
    self.zoomButton.selected = YES;

    MKMapCamera *newCamera = [MKMapCamera camera];
    newCamera.centerCoordinate = self.mapView.camera.centerCoordinate;
    newCamera.heading = self.mapView.camera.heading;
    newCamera.altitude = 50;
    newCamera.pitch = 70;
    [self.mapView setCamera:newCamera animated:YES];
    
    [self showDroneCoordinateOnSmallMap:[self getDroneCoordinates]];
    self.readyForUpdate = YES;
    
    [self.smallMapView setUserTrackingMode:MKUserTrackingModeNone];
    [self.mapView setUserTrackingMode:MKUserTrackingModeNone];
}

- (IBAction) startShowingUserHeading:(id)sender
{
    [self syncMapViewLocations];
    
}

- (IBAction)deleteKeyframePressed:(id)sender {
    FSDataManager *fsDataManager = [FSDataManager sharedManager];
    FSPathModelObject* currentPathModel = fsDataManager.currentPathModel;
    
    FSWaypointAnnotation *annotation = [currentPathModel getAnnotationForChosenWaypointIndex:self.waypointCollectionViewController.currentIndexPath.row];
    
    [self.mapView removeAnnotation:annotation];
    [self.smallMapView removeAnnotation:annotation];
    
    [currentPathModel.waypointArray removeObjectAtIndex:self.waypointCollectionViewController.currentIndexPath.row];
    [currentPathModel reshuffleAnnotations];
    for (id<MKAnnotation> annotation in self.mapView.annotations)
    {
        [self.mapView removeAnnotation:annotation];
        [self.smallMapView removeAnnotation:annotation];
        // change coordinates etc
        [self.mapView addAnnotation:annotation];
        [self.smallMapView addAnnotation:annotation];
    }
    
    [self.waypointCollectionViewController.collectionView reloadData];
    
    if (currentPathModel.waypointArray.count > 0) {
        self.deleteAllFramesButton.enabled = YES;
    } else {
        self.deleteAllFramesButton.enabled = NO;
    }
    self.deleteKeyframeButton.enabled = NO;
    self.editKeyFrameButton.enabled = NO;

    self.editFramePressed = NO;
    self.editAltitudePressed = NO;
    self.editPitchAttitudePressed = NO;

}

- (IBAction)deleteAllPressed:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Are you sure you want to delete all Keyframes?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"YES", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self deleteAllAction];
                               }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction * __nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:^{
        }];
    });
    
}

- (void)deleteAllAction
{
    FSDataManager *fsDataManager = [FSDataManager sharedManager];
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.smallMapView removeAnnotations:self.smallMapView.annotations];

    FSPathModelObject* currentPathModel = fsDataManager.currentPathModel;
    
    [currentPathModel.waypointArray removeAllObjects];
    
    [self.waypointCollectionViewController.collectionView reloadData];
    
    self.deleteKeyframeButton.enabled = NO;
    self.editKeyFrameButton.enabled = NO;
    self.deleteAllFramesButton.enabled = NO;

    self.editFramePressed = NO;
    self.editAltitudePressed = NO;
    self.editPitchAttitudePressed = NO;
    
    [Flurry logEvent:@"Delete All Frames"];
    
    if (self.userLocation != nil ) {
        [self.mapView addAnnotation:self.userLocation]; // will cause user location pin to blink
        [self.smallMapView addAnnotation:self.userLocation]; // will cause user location pin to blink
    }
    
    if (self.aircraftAnnotation != nil) {
        [self.smallMapView addAnnotation:self.aircraftAnnotation]; // will cause user location pin to blink
    }
    
    [self resetScreenParameters];
}


- (IBAction)updateKeyframe:(id)sender {
    __block FSDataManager *fsDataManager = [FSDataManager sharedManager];
    __block FSPathModelObject* currentPathModel = fsDataManager.currentPathModel;

//    self.altitude = [FSHelper getDroneAltitudeForMapCameraAltitude:self.mapView.camera.altitude];;
//    self.stayTime = [self.stayTimeTextField.text integerValue];
//    self.horizontalVelocity = [self.hSpeedTextField.text integerValue];

    CLLocationCoordinate2D droneCoordinates = [self getDroneCoordinates];

    // Place a single pin
    __block FSWaypointAnnotation *updatedAnnotation = [[FSWaypointAnnotation alloc] initWithCoordinate:droneCoordinates andWaypointLabel:@""];
    [updatedAnnotation setCoordinate:droneCoordinates];

    __block FSWaypointAnnotation *annotationToDelete = [currentPathModel getAnnotationForChosenWaypointIndex:self.waypointCollectionViewController.currentIndexPath.row];

    [self.mapView removeAnnotation:annotationToDelete];
    [self.smallMapView removeAnnotation:annotationToDelete];
    
    CLLocationDistance droneAltitude = [FSHelper getDroneAltitudeForMapCameraAltitude:self.mapView.camera.altitude];
    // Flurry
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [NSString stringWithFormat:@"%f", droneCoordinates.latitude], @"DroneCoordinatesLat",
                                   [NSString stringWithFormat:@"%f", droneCoordinates.longitude], @"DroneCoordinatesLong",
                                   [NSString stringWithFormat:@"%f", droneAltitude], @"DroneAltitude",
                                   [NSString stringWithFormat:@"%f", self.mapView.camera.heading], @"DroneHeading",nil];
    
    [Flurry logEvent:@"Add Waypoint" withParameters:articleParams];

    
    __block MKMapCamera *camera = [self.mapView.camera copy];

    WeakSelf weakSelf = self;
    [self.waypointCollectionViewController animateCurrentlySelectedCell:YES];
    [self snapShotCurrentMapWithCompletion:^(NSData *imageData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            StrongSelf strongSelf = weakSelf;
            
            if (strongSelf) {

                [self.waypointCollectionViewController animateCurrentlySelectedCell:NO];
                updatedAnnotation.waypointLabel = [FSHelper getAlphaCharacter:(int)strongSelf.waypointCollectionViewController.currentIndexPath.row];

                NSLog(@"Saved Lat: %f Long: %f DroneLat: %f DroneLong: %f Altitude: %f Pitch: %f", camera.centerCoordinate.latitude, camera.centerCoordinate.longitude, droneCoordinates.latitude, droneCoordinates.longitude, camera.altitude, camera.pitch);

                
                CGFloat droneAltitudeInMeters = [FSHelper getDroneAltitudeForMapCameraAltitude:camera.altitude];
                CGFloat dronePitch = [FSHelper convertToDJIAngle:camera.pitch];

                if ([strongSelf.droneHeightLabel.textColor isEqual:[UIColor colorWithRed:0.29 green:0.56 blue:0.88 alpha:1.0]]) {
                    droneAltitudeInMeters = strongSelf.droneAltitude * FEET_TO_METERS;
                } else if ([strongSelf.pitchLabel.textColor isEqual:[UIColor colorWithRed:0.29 green:0.56 blue:0.88 alpha:1.0]]) {
                    dronePitch = [FSHelper convertToCameraAngle:strongSelf.pitchAttitude];
                }

                [currentPathModel updateWaypointLocationForMapCamera:strongSelf.mapView.camera droneAltitude:droneAltitudeInMeters dronePitch:dronePitch withHorizontalVelocity:strongSelf.horizontalVelocity stayTime:strongSelf.stayTime lookingAtCoordinate:strongSelf.mapView.centerCoordinate forMapImageData:imageData forIndex:strongSelf.waypointCollectionViewController.currentIndexPath.row andAnnotation:updatedAnnotation];
                [updatedAnnotation setTitle:[NSString stringWithFormat:@"Altitude: %0.2f ft", strongSelf.droneAltitude]]; //You can set the subtitle too
            
                [strongSelf.mapView addAnnotation:updatedAnnotation];
                [strongSelf.smallMapView addAnnotation:updatedAnnotation];

                [strongSelf clearSelectionForWaypointCollectionViewController:nil];
                
                NSDictionary *dictionary = [FSHelper getEstimatedTimeForWaypointIndex:([currentPathModel.waypointArray count]-1)];
                NSNumber *estimatedTime = [dictionary objectForKey:@"estimatedTime"];
                NSNumber *estimatedBattery = [dictionary objectForKey:@"estimatedBatteryUsage"];
                NSNumber *totalDistance = [dictionary objectForKey:@"totalDistance"];
                
                NSString *estimatedTimeString = [FSHelper convertTimeToString:[estimatedTime floatValue]];
                
                strongSelf.estimatedBatteryLabel.text = [NSString stringWithFormat:@"EST. BATTERY %0.2f%%", [estimatedBattery floatValue]];
                strongSelf.totalTimeLabel.text = [NSString stringWithFormat:@"TOTAL TIME %@", estimatedTimeString];
                self.totalDistanceLabel.text = [NSString stringWithFormat:@"TOTAL DISTANCE %0.2fft", ([totalDistance floatValue] * METERS_TO_FEET)];
            }
        });
    }];

}

- (IBAction)searchNewLocationPressed:(id)sender {
    [self performSegueWithIdentifier:@"locateSegue" sender:self];
}

- (IBAction)hybridButtonPressed:(id)sender {
    if (self.smallMapView.mapType == MKMapTypeStandard) {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 90000
        self.smallMapView.mapType = MKMapTypeHybridFlyover;
#else
        self.smallMapView.mapType = MKMapTypeHybrid;
#endif
        return;
    }
    
    self.smallMapView.mapType = MKMapTypeStandard;
}


- (IBAction)zoomButtonPressed:(id)sender {
    UIButton *button = (UIButton *)sender;

    if (!button.selected) {
        [self zoomSmallMap];
    } else {
        [self resetSmallMap];
    }
    
    //Toggle current state and save
    button.selected = !button.selected;
    
}

- (IBAction)playbackButtonPressed:(id)sender {
    FSDataManager *fsDataManager = [FSDataManager sharedManager];
    FSPathModelObject* currentPathModel = fsDataManager.currentPathModel;
    
    if ([currentPathModel.waypointArray count] == 0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Nothing to playback" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }

    [self setPlayStatus:YES];
    
    if(self.waypointCollectionViewController.currentIndexPath == nil)
    {
        [self flyToLocation:nil];
    } else {
        FSWaypointModelObject *model = [currentPathModel.waypointArray objectAtIndex:self.waypointCollectionViewController.currentIndexPath.row];
        [self flyToLocation:model];
    }
    
}

- (IBAction)pauseButtons:(id)sender {
    [self setPlayStatus:NO];
    [self.view bringSubviewToFront:self.playButton];
}

- (IBAction)missionsButtonPressed:(id)sender {
    if (([FSDataManager sharedManager].currentPathModel.waypointArray.count == 0)||(![[FSDataManager sharedManager].currentPathModel.pathName isEqualToString:@""])) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Don't you want to save your Mission?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"YES", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self saveMissionPressed:nil];
                               }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction * __nonnull action) {
        [self dismissViewControllerAnimated:NO completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)saveMissionPressed:(id)sender {
    if ([FSDataManager sharedManager].currentPathModel.waypointArray.count == 0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Nothing to save" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }

    [self performSegueWithIdentifier:@"saveMissionSegue" sender:self];
    [self snapShotMapData];
}

#pragma mark FSSearchViewControllerDelegate

- (void)searchForNewLocation:(NSString *)locationString withPlaceMark:(CLPlacemark *)placemark
{
    if ([locationString isEqualToString:@""]) {
        return;
    }
    
    if ([locationString isEqualToString:MY_CURRENT_LOCATION]) {
        [self syncMapViewLocations];
        return;
    }
    
    
    if (placemark != nil) {
        float spanX = 0.00725;
        float spanY = 0.00725;
        MKCoordinateRegion region;
        region.center.latitude = placemark.location.coordinate.latitude;
        region.center.longitude = placemark.location.coordinate.longitude;
        region.span = MKCoordinateSpanMake(spanX, spanY);
        [self.mapView setRegion:region animated:YES];
    }
}


@end
