//
//  AppDelegate.m
//  CoPilot
//
//  Created by Pronob Ashwin on 6/2/15.
//  Copyright (c) 2015 FreeSkies. All rights reserved.
//

#import "AppDelegate.h"
#import <DJISDK/DJISDK.h>
#import <Parse/Parse.h>
#import "Flurry.h"
#import "FSEnableDroneIAPHelper.h"

@interface AppDelegate () 

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

    // IAP stuff
    [FSEnableDroneIAPHelper sharedInstance];
    
    // DJI init
    // Flurry Init

#if COPILOTORG // org.freeskies.copilot
    NSString *appKey = @"ff4a04a3405a08ee4d9e01f7";
    [Flurry startSession:@"S2CMQZ8MPTWMCYK4Q7ZC"];
#else // IAP version com.freeskies.copilot
    NSString *appKey = @"1005918c42ee59fb147c09d4";
    [Flurry startSession:@"WT5MZM9QM6N48K2RSTP3"];
#endif
    [DJIAppManager registerApp:appKey withDelegate:self];
    // End DJI init
    
    
    
    // Parse init
    
    // Initialize Parse init
    [Parse setApplicationId:@"nvtsljo5s1rvmhfqskEU4J0Xjtgyzl6L8g8IysIp"
                  clientKey:@"niSuAjiNkL5sgjfS7wBwIu3Ok7SrnfHGfKEDCMcy"];
    
    // [Optional] Track statistics around application opens.
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    // End Parse init
    
    return YES;
}

-(void) appManagerDidRegisterWithError:(int)error
{
    NSString* message = @"Register App Succedded!";
    if (error != RegisterSuccess) {
        message = @"Register App Failed!";
        // TBD show toast notification instead
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Failed to register with the DJI Server. Please contact FreeSkies to get a new app." message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
