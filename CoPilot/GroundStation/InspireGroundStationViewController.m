//
//  GroundStationTestViewController.m
//  DJIVisionSDK
//
//  Created by Ares on 14-7-16.
//  Copyright (c) 2014年 DJI. All rights reserved.
//

#import "InspireGroundStationViewController.h"
#import "FSDataManager.h"
#import "FSWaypointModelObject.h"
#import "VideoPreviewer.h"
#import "FSWebViewViewController.h"
#import "FSHelper.h"
#import "SourceForPageViewController.h"
#import "FSGlobalDefines.h"
#import "Flurry.h"
#import "WaypointAnnotationView.h"
#import "DJIAircraftAnnotation.h"
#import "DJIAircraftAnnotationView.h"
#import "DJIWaypointAnnotation.h"
#import "DJIWaypointAnnotationView.h"

#import "FSEnableDroneIAPHelper.h"

#define SIMULATION 0

#define DeviceSystemVersion ([[[UIDevice currentDevice] systemVersion] floatValue])
#define iOS8System (DeviceSystemVersion >= 8.0)


#define DEGREE_OF_THIRTY_METER (0.0000899322 * 3)
#define DEGREE(x) ((x)*180.0/M_PI)

@interface InspireGroundStationViewController ()  <SourceForPageViewControllerDelegate, DJIRemoteControllerDelegate, DJIImageTransmitterDelegate, MKMapViewDelegate>

@property (strong, nonatomic) UIView *videoPreviewView;
@property (strong, nonatomic) DJIInspireCamera* mInspireCamera;

@property (strong, nonatomic) NSDate *startDate;

@property (nonatomic) BOOL missionPaused;

@property(nonatomic, strong) DJIWaypointAnnotation* homeAnnotation;
@property(nonatomic, strong) DJIAircraftAnnotation* aircraftAnnotation;

@property (strong, nonatomic) DJICameraSystemState* mCameraSystemState;
@property (strong, nonatomic) DJICameraPlaybackState* mCameraPlaybackState;
@property (nonatomic) DJIRCHardwareState mLastHardwareState;
@property (nonatomic) DJIRCBatteryInfo mRCBatteryInfo;
@property (strong, nonatomic) DJIImageTransmitter* mImageTransmitter;
@property (strong, nonatomic) DJIMCSystemState *state;
@property (strong, nonatomic) DJIGroundStationFlyingInfo* flyingInfo;

@property (strong, nonatomic) FSWebViewViewController *fsWebViewViewController;
@property (strong, nonatomic) SourceForPageViewController *sourceForPageViewController;
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;

@property (nonatomic) CameraWorkMode mLastWorkMode;
@property (weak, nonatomic) IBOutlet MKMapView *droneMapView;
@property (weak, nonatomic) IBOutlet UIImageView *droneLocationImageView;
@property (weak, nonatomic) IBOutlet UILabel *connectionStatusLabel;

@property (weak, nonatomic) IBOutlet UILabel *totalTimeTextField;
@property (weak, nonatomic) IBOutlet UILabel *distanceTextField;
@property (weak, nonatomic) IBOutlet UILabel *hSpeedTextField;
@property (weak, nonatomic) IBOutlet UILabel *vSpeedTextField;
@property (weak, nonatomic) IBOutlet UILabel *videoRecordLabel;
@property (weak, nonatomic) IBOutlet UIImageView *gpsImageView;

@property (weak, nonatomic) IBOutlet UIView *statusView;

@property (weak, nonatomic) IBOutlet UILabel *cameraStrengthTextField;
@property (weak, nonatomic) IBOutlet UILabel *controllerStrengthTextField;
@property (weak, nonatomic) IBOutlet UILabel *gpsStrengthTextField;

@property (weak, nonatomic) IBOutlet UISwitch *photoVideoSwitch;

@property (nonatomic, strong) IBOutlet UIView* playbackBtnsView;
@property (weak, nonatomic) IBOutlet UIButton *playVideoBtn;
@property (weak, nonatomic) IBOutlet UIButton *prevMediaButton;
@property (weak, nonatomic) IBOutlet UIButton *nextMediaButton;

@property (weak, nonatomic) IBOutlet UILabel *heightTextField;
@property (weak, nonatomic) IBOutlet UILabel *headingTextField;
@property (weak, nonatomic) IBOutlet UILabel *pitchTextField;
@property (weak, nonatomic) IBOutlet UILabel *batteryLifeTextField;
@property (weak, nonatomic) IBOutlet UIButton *waypointButton;

@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property (strong, nonatomic) NSTimer* readBatteryInfoTimer;
@property (strong, nonatomic) NSTimer* totalMissionTimeInfoTimer;
@property (strong, nonatomic) NSTimer* messageTimer;

@property (weak, nonatomic) IBOutlet UIView *weatherView;
@property (weak, nonatomic) IBOutlet UIImageView *weatherImageView;

@property (weak, nonatomic) IBOutlet UIButton *recordVideoButton;
@property (weak, nonatomic) IBOutlet UIButton *capturePhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *playbackVideoButton;


@property (strong, nonatomic) DJIInspireRemoteController* mRemoteController;

@property (nonatomic) enum PreFlightMode preFlightMode;
@property (nonatomic) enum FlightMode flightMode;

@property (weak, nonatomic) IBOutlet UIView *messageView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *pauseButton;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;

@property(nonatomic, copy) void (^completionBlock)(BOOL completion);


// Video playback screen

@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIButton *stopPlaybackButton;
@property (weak, nonatomic) IBOutlet UIButton *showGridButton;
@property (weak, nonatomic) IBOutlet UIButton *goBackToFPVScreenButton;

// IAP stuff

@property (strong, nonatomic) NSArray *products;

@end

@implementation InspireGroundStationViewController

-(void) setDroneType:(DJIDroneType)type
{
    _drone = [[DJIDrone alloc] initWithType:type];
    _drone.delegate = self;
    _drone.camera.delegate = self;

    self.navigationManager = _drone.mainController.navigationManager;
    self.navigationManager.delegate = self;
    _drone.mainController.mcDelegate = self;
    self.waypointMission = self.navigationManager.waypointMission;
    
    _mRemoteController = (DJIInspireRemoteController*)_drone.remoteController;
    _mRemoteController.delegate = self;
    
    _mInspireMainController = (DJIInspireMainController*)_drone.mainController;
    _mInspireMainController.mcDelegate = self;

    self.mInspireCamera = (DJIInspireCamera*)_drone.camera;
    self.mLastWorkMode = CameraWorkModeUnknown;
    
    _mImageTransmitter = _drone.imageTransmitter;
    _mImageTransmitter.delegate = self;
    self.flightMode = FLIGHTMODE_AUTO;
    
    _mGimbal = (DJIPhantom3AdvancedGimbal*)_drone.gimbal;
    _mGimbal.delegate = self;

}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    self.playVideoBtn.hidden = YES;
    self.prevMediaButton.hidden = YES;
    self.nextMediaButton.hidden = YES;
    

    self.flightModeDict = @{ [NSNumber numberWithInt:FLIGHTMODE_AUTO] : @[@"AUTO MODE - Auto Pilot + No Camera Control", @"A Mode", @YES],
                             [NSNumber numberWithInt:FLIGHTMODE_PITCH] : @[@"PITCH MODE - Auto Pilot + Camera Pitch Control", @"P Mode", @YES],
                             [NSNumber numberWithInt:FLIGHTMODE_CAMERA] : @[@"CAMERA MODE - Auto Pilot + Camera Yaw and Pitch Control", @"C Mode", @YES],
                             [NSNumber numberWithInt:FLIGHTMODE_MANUAL] : @[@"MANUAL MODE - Full Flight and Camera Control", @"M Mode", @NO]};
    
    [self setDroneType:DJIDrone_Inspire];
    
    [self.droneMapView setUserTrackingMode:MKUserTrackingModeNone];
    self.droneMapView.showsBuildings = YES;
    self.droneMapView.showsPointsOfInterest = NO;
    self.droneMapView.showsUserLocation = YES;
    self.droneMapView.rotateEnabled = NO;

    UIImage *gpsImage = self.gpsImageView.image;
    gpsImage = [gpsImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.gpsImageView.image = gpsImage;
    
    _isPOIMissionStarted = NO;
    _isPOIMissionPaused = NO;
    
    _connectionStatusLabel.text = @"DISCONNECTED";
    [self setStatusUIToStatus:MODE_RED];

//    self.photoVideoSwitch.transform=CGAffineTransformRotate(self.photoVideoSwitch.transform,270.0/180*M_PI);

//    self.statusLabelLeft = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.scrollViewLeft.frame.size.width - 20, 300)];
//    self.statusLabelLeft.textAlignment = NSTextAlignmentLeft;
//    self.statusLabelLeft.font = [UIFont systemFontOfSize:12];
//    self.statusLabelLeft.numberOfLines = 0;
//    self.statusLabelLeft.text = @"N/A";
//    self.scrollViewLeft.backgroundColor = [UIColor lightGrayColor];
//    [self.scrollViewLeft addSubview:self.statusLabelLeft];
//    
    self.videoPreviewView = [[UIView alloc] initWithFrame:CGRectMake(0, 52, SCREEN_WIDTH, SCREEN_HEIGHT - 70)];
    [self.view addSubview:self.videoPreviewView];
    [self.view sendSubviewToBack:self.videoPreviewView];
    self.videoView.hidden = YES;
    
    self.videoPreviewView.backgroundColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.0];
    [[VideoPreviewer instance] start];
    
    self.capturePhotoButton.hidden = YES;
    self.recordVideoButton.hidden = NO;
    self.videoRecordLabel.hidden = NO;
    
    [self getCameraWorkMode];
    self.missionPaused = NO;

    if ([FSDataManager sharedManager].currentPathModel != nil) {
        [self addAnnotationsFromModel:[FSDataManager sharedManager].currentPathModel];
    }

#ifdef IAP
    [self IAPSetup];
#else
    self.buyButton.enabled = NO;
    self.buyButton.hidden = YES;
#endif
    
}

#ifdef IAP
#pragma mark - IAP

- (void)showStartUpMessage
{
    NSString *message = @"Notice: CoPilot Basic now supports an In-app purchase. Check maps, set keyframes, preview your mission, and fly manually in M-mode. If you would like autonomous control, you will be asked to purchase the full app during launch. If you have purchased CoPilot before this update, please email info@freeskies.co before Dec 15th, 2015 to enable full access.";
    
    UIAlertController *messageAlertController = [UIAlertController alertControllerWithTitle:@"Message" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [messageAlertController addAction:okAction];
    [self presentViewController:messageAlertController animated:YES completion:nil];
    
}

- (void)IAPSetup
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    
    self.products = nil;
    
    WeakSelf weakSelf = self;
    
    [[FSEnableDroneIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            
            StrongSelf strongSelf = weakSelf;
            if (strongSelf) {
                strongSelf.products = products;
                if (strongSelf.products.count > 0) {
                    SKProduct *product = (SKProduct *) strongSelf.products[0];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [strongSelf setButtonWithProduct:product];
                    });
                }
            }
        }
    }];
}

- (void)setButtonWithProduct:(SKProduct *)product
{
    // Add new instance variable to class extension
    NSNumberFormatter * _priceFormatter;
    
    // Add to end of viewDidLoad
    _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    // Add to bottom of tableView:cellForRowAtIndexPath (before return cell)
    [_priceFormatter setLocale:product.priceLocale];
    
    [self.buyButton setTitle:[_priceFormatter stringFromNumber:product.price] forState:UIControlStateNormal];
    [self.buyButton setImage:nil forState:UIControlStateNormal];
    
    if ([[FSEnableDroneIAPHelper sharedInstance] productPurchased:product.productIdentifier]) {
        // product purchased
        self.buyButton.enabled = NO;
        self.buyButton.hidden = YES;
    } else {
        [self showStartUpMessage];
        self.buyButton.enabled = YES;
        self.buyButton.hidden = NO;
    }

}

- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    [self.products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            self.buyButton.enabled = NO;
            self.buyButton.hidden = YES;
            *stop = YES;
        }
    }];
    
}

#endif

#pragma mark - Guestures

-(void)setUpGestureRecognizers
{
    self.swipeLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftGestureAction:)];
    self.swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    self.swipeRightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRightGestureAction:)];
    self.swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self.view addGestureRecognizer:self.swipeLeftGesture];
    [self.view addGestureRecognizer:self.swipeRightGesture];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[VideoPreviewer instance] setView:self.videoPreviewView];
    [_drone connectToDrone];
    [self.mInspireMainController startUpdateMCSystemState];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}


-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_drone.camera startCameraSystemStateUpdates];

    MKCoordinateRegion region;
    region.center = self.droneMapView.userLocation.location.coordinate;
    region.span = MKCoordinateSpanMake(0.00025, 0.00040);
    [self.droneMapView setRegion:region animated:YES];

}


-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
#ifdef IAP
    [[NSNotificationCenter defaultCenter] removeObserver:self];
#endif
    
    [self.navigationController setNavigationBarHidden:NO];
    
    [self stopBatterUpdate];
    [self stopMessageTimer];
    [self.mImageTransmitter stopChannelPowerUpdatesWithResult:nil];

    [_drone.camera stopCameraSystemStateUpdates];
    [[VideoPreviewer instance] setView:nil];
    [self.mInspireMainController stopUpdateMCSystemState];
    [_drone disconnectToDrone];
    
    if (self.totalMissionTimeInfoTimer) {
        [self.totalMissionTimeInfoTimer invalidate];
        self.totalMissionTimeInfoTimer = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)addAnnotationsFromModel:(FSPathModelObject *)currentPathModel
{
    for (FSWaypointModelObject *waypointModel in currentPathModel.waypointArray) {
        if (waypointModel.waypointAnnotation) {
            [self.droneMapView addAnnotation:waypointModel.waypointAnnotation];
        }
    }
}


- (void)setStatusUIToStatus:(StatusMode)mode
{
    switch (mode) {
        case MODE_RED:
            _connectionStatusLabel.textColor = [UIColor redColor];
            _statusImage.image = [UIImage imageNamed:@"RedIcon"];
            _statusView.backgroundColor = [UIColor redColor];
            break;
            
        case MODE_YELLOW:
            _connectionStatusLabel.textColor = [UIColor yellowColor];
            _statusImage.image = [UIImage imageNamed:@"YellowIcon"];
            _statusView.backgroundColor = [UIColor yellowColor];
            break;

        default:
            _connectionStatusLabel.textColor = [UIColor greenColor];
            _statusImage.image = [UIImage imageNamed:@"GreenIcon"];
            _statusView.backgroundColor = [UIColor greenColor];
            break;
    }
}


#pragma mark mapview delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id < MKAnnotation >)annotation
{
    if([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
    
    if ([annotation isKindOfClass:[DJIWaypointAnnotation class]]) {
        static NSString* homepointReuseIdentifier = @"DJI_HOME_POINT_ANNOTATION_VIEW";

        DJIWaypointAnnotation* wpAnnotation = (DJIWaypointAnnotation*)annotation;
        DJIWaypointAnnotationView* annoView = (DJIWaypointAnnotationView*)[self.droneMapView dequeueReusableAnnotationViewWithIdentifier:homepointReuseIdentifier];
        if (annoView == nil) {
            annoView = [[DJIWaypointAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:homepointReuseIdentifier];
        }
        annoView.titleLabel.text = wpAnnotation.text;
        return annoView;
    }
    else if ([annotation isKindOfClass:[FSWaypointAnnotation class]])
    {
        FSWaypointAnnotation *waypointAnnotation  = (FSWaypointAnnotation *)annotation;
        WaypointAnnotationView *pinView = (WaypointAnnotationView*)[self.droneMapView dequeueReusableAnnotationViewWithIdentifier:@"AnnotationID"];
        
        if (!pinView) {
            pinView = [[WaypointAnnotationView alloc] initWithAnnotation:waypointAnnotation reuseIdentifier:@"AnnotationID"];
        }
        
        
        [self configureAnnotationView:pinView viewForAnnotation:waypointAnnotation];
        return pinView;
    }
    else if ([annotation isKindOfClass:[DJIAircraftAnnotation class]])
    {
        static NSString* aircraftReuseIdentifier = @"DJI_AIRCRAFT_ANNOTATION_VIEW";
        DJIAircraftAnnotationView* aircraftAnno = (DJIAircraftAnnotationView*)[self.droneMapView dequeueReusableAnnotationViewWithIdentifier:aircraftReuseIdentifier];
        if (aircraftAnno == nil) {
            aircraftAnno = [[DJIAircraftAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:aircraftReuseIdentifier];
        }
        return aircraftAnno;
    }
    return nil;
}

- (void)configureAnnotationView:(MKAnnotationView *)annotationView viewForAnnotation:(id < MKAnnotation >)annotation
{
    FSWaypointAnnotation *waypointAnnotation = (FSWaypointAnnotation *)annotation;
    
    UILabel *labelView = (UILabel *)[annotationView viewWithTag:1];
    
    if (labelView == nil)
    {
        //create and add label...
        labelView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        labelView.textAlignment = NSTextAlignmentCenter;
        labelView.tag = 1;
        labelView.textColor = [UIColor whiteColor];
        [annotationView addSubview:labelView];
    }
    
    labelView.text = waypointAnnotation.waypointLabel;
}


#pragma mark Segues

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"showLaunchWizardSegue"]) {
        if ([FSDataManager sharedManager].currentPathModel.waypointArray.count < 2) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"You need to have a minimum of 2 waypoint to launch a mission..." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"OK action");
                                       }];
            
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
            return NO;
        }
    }
    return YES;
    
}

- (void)prepareForSegue:(nonnull UIStoryboardSegue *)segue sender:(nullable id)sender
{
    if ([segue.identifier isEqualToString:@"weatherSegue"]) {
        self.fsWebViewViewController = segue.destinationViewController;
        self.fsWebViewViewController.webURLString = @"http://www.weather.com/";
    }
    
    if ([segue.identifier isEqualToString:@"noFlySegue"]) {
        self.fsWebViewViewController = segue.destinationViewController;
        self.fsWebViewViewController.webURLString = @"https://www.mapbox.com/drone/no-fly/";
    }
    
    if ([segue.identifier isEqualToString:@"showLaunchWizardSegue"]) {
        self.sourceForPageViewController = segue.destinationViewController;
        self.sourceForPageViewController.delegate = self;
    }
}



#pragma mark - User Action

#ifdef IAP
- (IBAction)buyButtonTapped:(id)sender {
    UIButton *buyButton = (UIButton *)sender;
    SKProduct *product = (SKProduct *) self.products[0];
    
    NSLog(@"Buying %@...", product.productIdentifier);
    [[FSEnableDroneIAPHelper sharedInstance] buyProduct:product];
    
}
#endif

- (IBAction)playVideoBtnAction:(id)sender {
    if (self.mCameraPlaybackState.mediaFileType == MediaFileVIDEO) {
        [self.mInspireCamera startVideoPlayback];
    }
}

- (IBAction)stopVideoBtnAction:(id)sender {
    if (self.mCameraPlaybackState.mediaFileType == MediaFileVIDEO) {
        if (self.mCameraPlaybackState.videoPlayProgress > 0) {
            [self.mInspireCamera stopVideoPlayback];
        }
    }
}

- (IBAction)startVideoRecording:(id)sender {
    [Flurry logEvent:@"Video Record Pressed"];
    if (self.mCameraSystemState.workMode != CameraWorkModeRecord) {
        [self setTimerMessage:@"Camera mode error. Press the record button on the controller first."];
        return;
    }
    
    if (self.mCameraSystemState.isRecording) {
        [_drone.camera stopRecord:^(DJIError *error) {
            [self.recordVideoButton setImage:[UIImage imageNamed:@"RecordVideo"] forState:UIControlStateNormal];
        }];
    }
    else
    {
        [_drone.camera startRecord:^(DJIError *error) {
            [self.recordVideoButton setImage:[UIImage imageNamed:@"RecordVideoOn"] forState:UIControlStateNormal];
        }];
    }
}

- (IBAction)capturePhoto:(id)sender {
    if (self.mCameraSystemState) {
        if (self.mCameraSystemState.isTakingContinusCapture ||
            self.mCameraSystemState.isTakingMultiCapture) {
            [_drone.camera stopTakePhotoWithResult:^(DJIError *error) {
                [self setTimerMessage:[NSString stringWithFormat:@"Stop Take photo: %@", error.errorDescription]];
            }];
        }
        else
        {
            if (!self.mCameraSystemState.isSDCardExist) {
                [self setTimerMessage:@"Please insert a SD Card..."];
                return;
            }
            if (self.mCameraSystemState.workMode != CameraWorkModeCapture) {
                [self setTimerMessage:@"Camera mode error. Press the record button on the controller first."];
                return;
            }
            if (self.mCameraSystemState.isTakingSingleCapture ||
                self.mCameraSystemState.isTakingRawCapture) {
                [self setTimerMessage:@"Camera is Busy..."];
                return;
            }
            if (self.mCameraSystemState.isSeriousError || self.mCameraSystemState.isCameraSensorError) {
                [self setTimerMessage:@"Camera system error..."];
                return;
            }
            
            CameraCaptureMode mode = CameraSingleCapture;
            [self.capturePhotoButton setBackgroundImage:[UIImage imageNamed:@"PhotoCaptured"] forState:UIControlStateNormal];
            
            WeakSelf weakself = self;
            [_drone.camera startTakePhoto:mode withResult:^(DJIError *error) {
                NSLog(@"Take photo:%@", error.errorDescription);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakself.capturePhotoButton setBackgroundImage:[UIImage imageNamed:@"TakePhoto"] forState:UIControlStateNormal];
                });
            }];
        }
    }
    else
    {
        CameraCaptureMode mode = CameraSingleCapture;
        [_drone.camera startTakePhoto:mode withResult:^(DJIError *error) {
            NSLog(@"Try Take photo:%@", error.errorDescription);
        }];
    }
}

- (IBAction)photoVideoSwitchPressed:(id)sender {
    UISwitch *uiswitch = (UISwitch *)sender;
    
    if (uiswitch.on) {
        [self setVideoMode:CameraWorkModeRecord];
    } else {
        [self setVideoMode:CameraWorkModeCapture];
    }

}

- (IBAction)playBackPressed:(id)sender {
    [self setVideoMode:CameraWorkModePlayback];
}



-(IBAction) onOpenButtonClicked:(id)sender
{
    [self.navigationManager enterNavigationModeWithResult:^(DJIError *error) {
        if (error.errorCode == ERR_Succeeded) {
            [self.connectButton setTitle:@"AUTO PILOT ENABLED" forState:UIControlStateNormal];
            [self.connectButton setBackgroundColor:[UIColor redColor]];
        }
        [self setTimerMessage:[NSString stringWithFormat:@"Enter Navigation Mode:%@", error.errorDescription]];
    }];
}

-(IBAction) onCloseButtonClicked:(id)sender
{
    [self.navigationManager exitNavigationModeWithResult:^(DJIError *error) {
        [self setTimerMessage:[NSString stringWithFormat:@"Exit Navigation Mode:%@", error.errorDescription]];
    }];
}


#pragma misc

- (void)setVideoMode:(CameraWorkMode)mode
{
    WeakSelf weakSelf = self;
    
    switch (mode) {
        case CameraWorkModeRecord: {
            [self.mInspireCamera setCameraWorkMode:CameraWorkModeRecord withResult:^(DJIError *error) {
                if (weakSelf) {
                    StrongSelf strongSelf = weakSelf;
                    if (strongSelf) {
                        strongSelf.recordVideoButton.hidden = NO;
                        strongSelf.videoRecordLabel.hidden = NO;
                        strongSelf.capturePhotoButton.hidden = YES;
                    }
                }
            }];
            break;
        }
            
        case CameraWorkModeCapture: {
            [self.mInspireCamera setCameraWorkMode:CameraWorkModeCapture withResult:^(DJIError *error) {
                if (weakSelf) {
                    StrongSelf strongSelf = weakSelf;
                    if (strongSelf) {
                        strongSelf.recordVideoButton.hidden = YES;
                        strongSelf.videoRecordLabel.hidden = YES;
                        strongSelf.capturePhotoButton.hidden = NO;
                    }
                }
            }];
            break;
        }
            
        case CameraWorkModePlayback: {
            [self.mInspireCamera setCameraWorkMode:CameraWorkModePlayback withResult:^(DJIError *error) {
                NSLog(@"Enter Playback:%@", error.errorDescription);
                
                StrongSelf strongSelf = weakSelf;
                
                if (strongSelf) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[VideoPreviewer instance] setView:strongSelf.videoView];
                        strongSelf.videoView.hidden = NO;
                        [strongSelf.view bringSubviewToFront:strongSelf.videoView];
                        
                        strongSelf.mLastWorkMode = strongSelf.mCameraSystemState.workMode;

                        strongSelf.prevMediaButton.hidden = NO;
                        strongSelf.nextMediaButton.hidden = NO;
                    });
                }
            }];
            break;
        }
            
        default:
            break;
    }
}

-(float) getCornerRadius:(DJIWaypoint*)previous middleWaypoint:(DJIWaypoint*)middle nextWaypoint:(DJIWaypoint*)next
{
    if (previous == nil || middle == nil || next == nil) {
        return 2.0;
    }
    CLLocation* loc1 = [[CLLocation alloc] initWithLatitude:previous.coordinate.latitude longitude:previous.coordinate.longitude];
    CLLocation* loc2 = [[CLLocation alloc] initWithLatitude:middle.coordinate.latitude longitude:middle.coordinate.longitude];
    CLLocation* loc3 = [[CLLocation alloc] initWithLatitude:next.coordinate.latitude longitude:next.coordinate.longitude];
    CLLocationDistance d1 = [loc2 distanceFromLocation:loc1];
    CLLocationDistance d2 = [loc2 distanceFromLocation:loc3];
    CLLocationDistance dmin = MIN(d1, d2);
    if (dmin < 1.0) {
        dmin = 1.0;
    }
    else
    {
        dmin = 1.0 + (dmin - 1.0) * 0.2;
        dmin = MIN(dmin, 10.0);
    }
    return dmin;
}

-(void) calcDampingDistance
{
    for (int i = 0; i < self.waypointMission.waypointCount; i++) {
        DJIWaypoint* wp = (DJIWaypoint*)[self.waypointMission waypointAtIndex:i];
        DJIWaypoint* prevWaypoint = nil;
        DJIWaypoint* nextWaypoint = nil;
        int prev = i - 1;
        int next = i + 1;
        if (prev >= 0) {
            prevWaypoint = [self.waypointMission waypointAtIndex:prev];
        }
        if (next < self.waypointMission.waypointCount) {
            nextWaypoint = [self.waypointMission waypointAtIndex:next];
        }
        wp.cornerRadius = [self getCornerRadius:prevWaypoint middleWaypoint:wp nextWaypoint:nextWaypoint];
    }
}


-(void) updateMission
{
    self.waypointMission.maxFlightSpeed = 10.0;
    self.waypointMission.autoFlightSpeed = 5.0;
    self.waypointMission.finishedAction = DJIWaypointMissionFinishedGoHome;
    
    if (self.flightMode == FLIGHTMODE_CAMERA) {
        self.waypointMission.headingMode = DJIWaypointMissionHeadingControlByRemoteController;
    } else {
        self.waypointMission.headingMode = DJIWaypointMissionHeadingUsingWaypointHeading;
    }

    if ((self.flightMode == FLIGHTMODE_PITCH)||(self.flightMode == FLIGHTMODE_CAMERA)) {
        self.waypointMission.flightPathMode = DJIWaypointMissionFlightPathCurved;
        [self calcDampingDistance];
    } else {
        self.waypointMission.flightPathMode = DJIWaypointMissionFlightPathNormal;
    }
    
    [self.waypointMission removeAllWaypoints];
}


-(void) uploadTaskWithCompletion:(UploadTaskBlock)uploadTaskDone
{
    FSDataManager *fsDataManager = [FSDataManager sharedManager];
    
    
    MKCoordinateRegion region;
    region.center = CLLocationCoordinate2DMake(mCurrentDroneCoordinate.latitude, mCurrentDroneCoordinate.longitude);
    region.span = MKCoordinateSpanMake(0.00025, 0.00040);
    [self.droneMapView setRegion:region animated:YES];

    
    NSLog(@"CurrentCoordinate:{%f, %f}", mCurrentDroneCoordinate.latitude, mCurrentDroneCoordinate.longitude);
    if (CLLocationCoordinate2DIsValid(mCurrentDroneCoordinate)) {
        [self updateMission];
        
        for (FSWaypointModelObject *modelObject in fsDataManager.currentPathModel.waypointArray) {
            if (self.flightMode == FLIGHTMODE_PITCH) {
                DJIWaypoint *waypoint = modelObject.waypoint;
                
                for (DJIWaypointAction *action in waypoint.waypointActions) {
                    if (action.actionType == DJIWaypointActionRotateGimbalPitch) {
                        [waypoint removeAction:action];
                        break;
                    }
                }
            }
            [self.waypointMission addWaypoint:modelObject.waypoint];
            NSLog(@"Waypoint %@: Altitude: %0.2f m", modelObject.alphaLabel, modelObject.waypoint.altitude);
        }
        
        
        if (self.waypointMission.isValid) {
            WeakSelf weakSelf = self;
            [self.waypointMission setUploadProgressHandler:^(uint8_t progress) {
                StrongSelf strongSelf = weakSelf;
                
                if (strongSelf) {
                    NSString* message = [NSString stringWithFormat:@"Mission Uploading:%d%%", progress];
                    if (strongSelf.mProgressAlertView == nil) {
                        strongSelf.mProgressAlertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
                        [strongSelf.mProgressAlertView show];
                    }
                    else
                    {
                        [strongSelf.mProgressAlertView setMessage:message];
                    }
                    
                    if (progress == 100) {
                        [strongSelf.mProgressAlertView dismissWithClickedButtonIndex:0 animated:YES];
                        strongSelf.mProgressAlertView = nil;
                    }
                }
            }];
            [self.waypointMission uploadMissionWithResult:^(DJIError *error) {
                StrongSelf strongSelf = weakSelf;
                
                if (strongSelf) {
                    if (strongSelf.mProgressAlertView) {
                        [strongSelf.mProgressAlertView dismissWithClickedButtonIndex:0 animated:YES];
                        strongSelf.mProgressAlertView = nil;
                    }
                    [strongSelf.waypointMission setUploadProgressHandler:nil];
                    if (uploadTaskDone) {
                        uploadTaskDone(YES);
                    }
                }
            }];
        }
        else
        {
            [self setTimerMessage:@"Waypoint mission invalid!"];
        }
    }
    else
    {
        [self showMessage:@"Current Drone Location Invalid"];
    }
}

- (void)incFlightMode {
    
    if (self.flightMode == FLIGHTMODE_MANUAL) {
        self.flightMode = FLIGHTMODE_AUTO;
        return;
    }
    self.flightMode++;
}


- (IBAction)selectMissionButtonPressed:(id)sender {
    [self incFlightMode];
    
    NSArray *modeArray = [self.flightModeDict objectForKey:[NSNumber numberWithInt:self.flightMode]];
    
    [self setTimerMessage:[modeArray objectAtIndex:0]];
    [self.waypointButton setBackgroundImage:[UIImage imageNamed:[modeArray objectAtIndex:1]] forState:UIControlStateNormal];
    BOOL d = [[modeArray objectAtIndex:2] boolValue];
    [self enablePlay:d];
}



- (IBAction)hybridButtonPressed:(id)sender {
    if (self.droneMapView.mapType == MKMapTypeStandard) {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 90000
        self.droneMapView.mapType = MKMapTypeHybridFlyover;
#else
        self.droneMapView.mapType = MKMapTypeHybrid;
#endif
        return;
    }
    
    self.droneMapView.mapType = MKMapTypeStandard;
}


- (void)setPlay:(BOOL)play
{
    self.missionPaused = play;
    self.playButton.hidden = !play;
    self.pauseButton.hidden = play;
}

- (void)enablePlay:(BOOL)enable
{
    self.playButton.hidden = !enable;
    self.pauseButton.hidden = !enable;
}

- (void)setTimerMessage:(NSString *)messageString
{
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:_cmd withObject:messageString waitUntilDone:NO];
    }
    else
    {
        self.messageLabel.text = messageString;
        
        self.messageView.alpha = 1.0;
        [self.view bringSubviewToFront:self.messageView];
        
        [self stopMessageTimer];
        [self startMessageTimer];
    }
}

-(IBAction) onDownloadTaskClicked:(id)sender
{
    
    [self.waypointMission setDownloadProgressHandler:^(uint8_t progress) {
    }];
    [self.waypointMission downloadMissionWithResult:^(DJIError *error) {
        [self setTimerMessage:[NSString stringWithFormat:@"Download Mission Result:%@", error.errorDescription]];
    }];
}

-(IBAction) onStartTaskButtonClicked:(id)sender
{
    if (self.missionPaused) {
        WeakSelf weakSelf = self;
        [self.waypointMission resumeMissionWithResult:^(DJIError *error) {
            
            StrongSelf strongSelf = weakSelf;
            
            if (strongSelf) {
                if (error.errorCode == ERR_Succeeded) {
                    [strongSelf setPlay:NO];
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (SIMULATION) {
                            [weakSelf setPlay:NO];
                        } else {
                            ShowResult(@"Resume Mission failed: %@", error.errorDescription);
                        }
                    });
                }
            }
            
        }];
        return;
    }
    
    [self performSegueWithIdentifier:@"showLaunchWizardSegue" sender:self];
    
}

-(IBAction) onStopTaskButtonClicked:(id)sender
{
    WeakSelf weakSelf = self;
    [self.waypointMission stopMissionWithResult:^(DJIError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf setPlay:YES];
        });
        [self setTimerMessage:[NSString stringWithFormat:@"Stop Mission:%@", error.errorDescription]];
    }];
}

-(IBAction) onPauseTaskButtonClicked:(id)sender
{
    WeakSelf weakSelf = self;

    [self.waypointMission pauseMissionWithResult:^(DJIError *error) {
        StrongSelf strongSelf = weakSelf;
        
        if (strongSelf) {
            if (error.errorCode == ERR_Succeeded) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [strongSelf setPlay:YES];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (SIMULATION) {
                        [weakSelf setPlay:YES];
                    } else {
                        ShowResult(@"Pause Mission failed: %@", error.errorDescription);
                    }
                });
            }
        }
        
    }];
    
}


- (IBAction)goHomeButtonClicked:(id)sender {
//    [_groundStation gohome];
}

-(IBAction) onBackButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) showMessage:(NSString*)message
{
    WeakSelf weakSelf = self;
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:_cmd withObject:message waitUntilDone:NO];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf setTimerMessage:message];
        });
    }
}

#pragma mark - Timer

-(void)onTick:(NSTimer *)timer {
    NSLog(@"Tick");
    NSTimeInterval elapsedTime = [[NSDate date] timeIntervalSinceDate:self.startDate];
    
    self.totalTimeTextField.text = [NSString stringWithFormat:@"TOTAL TIME %@", [FSHelper convertTimeToString:elapsedTime]];
}

#pragma mark - GroundStation Result

- (void)getCameraWorkMode {
    WeakSelf weakSelf = self;
    
    [self.mInspireCamera getCameraWorkModeWithResult:^(CameraWorkMode workMode, DJIError *error) {
        StrongSelf strongSelf = weakSelf;

        if (strongSelf) {
            
            if (error.errorCode == ERR_Succeeded) {
                [strongSelf enablePhotoVideoButtons:YES];
                
                switch (workMode) {
                    case CameraWorkModeRecord: {
                        [[VideoPreviewer instance] setView:self.videoPreviewView];
                        strongSelf.photoVideoSwitch.on = YES;
                        strongSelf.capturePhotoButton.hidden = YES;
                        strongSelf.recordVideoButton.hidden = NO;
                        strongSelf.videoRecordLabel.hidden = NO;
                        break;
                    }
                        
                    case CameraWorkModeCapture: {
                        [[VideoPreviewer instance] setView:self.videoPreviewView];
                        strongSelf.photoVideoSwitch.on = NO;
                        strongSelf.capturePhotoButton.hidden = NO;
                        strongSelf.recordVideoButton.hidden = YES;
                        strongSelf.videoRecordLabel.hidden = YES;
                        break;
                    }
                        
                    case CameraWorkModePlayback: {
                        [[VideoPreviewer instance] setView:self.videoView];
                        self.videoView.hidden = NO;
                        [self.view bringSubviewToFront:self.videoView];
                        break;
                    }
                    default:
                        break;
                }
                
            }
            else
            {
                [strongSelf enablePhotoVideoButtons:NO];
            }
        }
    }];
}


- (void)enablePhotoVideoButtons:(BOOL)enabled
{
    self.photoVideoSwitch.enabled = enabled;
    self.capturePhotoButton.enabled = enabled;
    self.recordVideoButton.enabled = enabled;
    self.videoRecordLabel.enabled = enabled;
    self.playbackVideoButton.enabled = enabled;
}


-(NSString*) onGroundStationControlModeChanged:(GroundStationControlMode)mode
{
    NSString* ctrlMode = @"N/A";
    switch (mode) {
        case GSModeAtti:
        {
            ctrlMode = @"ATTI";
            break;
        }
        case GSModeGpsAtti:
        {
            ctrlMode = @"GPS";
            break;
        }
        case GSModeGpsCruise:
        {
            ctrlMode = @"GPS";
            break;
        }
        case GSModeWaypoint:
        {
            ctrlMode = @"WAYPOINT";
            break;
        }
        case GSModeGohome:
        {
            ctrlMode = @"GOHOME";
            break;
        }
        case GSModeLanding:
        {
            ctrlMode = @"LANDING";
            break;
        }
        case GSModePause:
        {
            ctrlMode = @"PAUSE";
            break;
        }
        case GSModeTakeOff:
        {
            ctrlMode = @"TAKEOFF";
            break;
        }
        case GSModeManual:
        {
            ctrlMode = @"MANUAL";
            break;
        }
        default:
            break;
    }
    return ctrlMode;
}

-(void) onGroundStationGpsStatusChanged:(GroundStationGpsStatus)status
{
    switch (status) {
        case GSGpsGood:
        {
            [self.gpsImageView setTintColor:[UIColor greenColor]];
            break;
        }
        case GSGpsWeak:
        {
            [self.gpsImageView setTintColor:[UIColor yellowColor]];
            break;
        }
        case GSGpsBad:
        {
            [self.gpsImageView setTintColor:[UIColor redColor]];
            break;
        }
        default:
            break;
    }
}

#pragma mark - DJINavigationDelegate

-(void) onNavigationMissionStatusChanged:(DJINavigationMissionStatus*)missionStatus
{
    if (missionStatus.missionType == DJINavigationMissionWaypoint) {
        DJIWaypointMissionStatus *djimissionStatus = (DJIWaypointMissionStatus *)missionStatus;
        
        if (djimissionStatus.isWaypointReached) {
            NSLog(@"Reach Waypoint:%d", djimissionStatus.targetWaypointIndex);
            NSString *alphaLabel = [FSHelper getAlphaCharacter:(int)djimissionStatus.targetWaypointIndex];
            
            [self setTimerMessage:[NSString stringWithFormat:@"Reached Waypoint: %@", alphaLabel]];
            
            [Flurry logEvent:[NSString stringWithFormat:@"Reach Waypoint:%ld", (long)djimissionStatus.targetWaypointIndex]];
            
            if (djimissionStatus.targetWaypointIndex == [FSDataManager sharedManager].currentPathModel.waypointArray.count-1) {
                [self setPlay:YES];
            }
        }
    }
}


#pragma mark -

-(void) mainController:(DJIMainController *)mc didUpdateSystemState:(DJIMCSystemState *)state
{
    
    mCurrentDroneCoordinate = state.droneLocation;
    if (!state.isMultipleFlightModeOpen) {
        if ([_drone.mainController isKindOfClass:[DJIInspireMainController class]]) {
            [(DJIInspireMainController*)_drone.mainController setMultipleFlightModeOpen:YES withResult:nil];
        }
        else if ([_drone.mainController isKindOfClass:[DJIPhantom3ProMainController class]])
        {
            [(DJIPhantom3ProMainController*)_drone.mainController setMultipleFlightModeOpen:YES withResult:nil];
        }
        else if ([_drone.mainController isKindOfClass:[DJIPhantom3AdvancedMainController class]])
        {
            [(DJIPhantom3AdvancedMainController*)_drone.mainController setMultipleFlightModeOpen:YES withResult:nil];
        }
    }
    
    if (CLLocationCoordinate2DIsValid(state.droneLocation)) {
        if (self.aircraftAnnotation == nil) {
            self.aircraftAnnotation = [[DJIAircraftAnnotation alloc] initWithCoordiante:state.droneLocation];
            [self.droneMapView addAnnotation:self.aircraftAnnotation];
            
            MKCoordinateRegion region = {0};
            region.center = state.droneLocation;
            region.span.latitudeDelta = 0.001;
            region.span.longitudeDelta = 0.001;
            
            [self.droneMapView setRegion:region animated:YES];
        }
        
        [self.aircraftAnnotation setCoordinate:state.droneLocation];
        double heading = RADIAN(state.attitude.yaw);
        DJIAircraftAnnotationView* annoView = (DJIAircraftAnnotationView*)[self.droneMapView viewForAnnotation:self.aircraftAnnotation];
        [annoView updateHeading:heading];
    }
    
    if (CLLocationCoordinate2DIsValid(state.homeLocation)) {
        if (self.homeAnnotation == nil) {
            self.homeAnnotation = [[DJIWaypointAnnotation alloc] initWithCoordiante:state.homeLocation];
            self.homeAnnotation.text = @"H";
            [self.droneMapView addAnnotation:self.homeAnnotation];
        }
        
        [self.homeAnnotation setCoordinate:state.homeLocation];
    }


    self.state = state;
    
    NSMutableString* statsSting = [[NSMutableString alloc] init];
    [statsSting appendFormat:@"did update Ssytem State %d", (int)state.flightMode];
    

    if (state.flightMode == MainControllerFlightModeJoystick) {
            NSLog(@"Joystick Mode");
        [statsSting appendFormat:@"Joystick Mode"];
    }
    if (state.flightMode == MainControllerFlightModeGPSHotPoint) {
            NSLog(@"Hot point mode");
        [statsSting appendFormat:@"Hot point mode"];
    }
    if (state.flightMode == MainControllerFlightModeGPSWaypoint) {
        [statsSting appendFormat:@"Waypoint mode"];
            NSLog(@"Waypoint mode");
    }
    
    self.gpsStrengthTextField.text = [NSString stringWithFormat:@"%d SAT", state.satelliteCount];
    
    if (state.isCompassError) {
        _connectionStatusLabel.text = @"CALIBRATE COMPASS";
        [self setStatusUIToStatus:MODE_RED];

        [statsSting appendFormat:@"CALIBRATE COMPASS"];
//        [self showMessage:@"Please Calibrate your Compass using the DJI Pilot App"];
    }
    
    if (state.isIMUPreheating) {
        _connectionStatusLabel.text = @"IMU PREHEATING";
        [self setStatusUIToStatus:MODE_YELLOW];
        [statsSting appendFormat:@"IMU PREHEATING"];
    }
    
    // Set DISTANCE
    CLLocation *newLocation = [[CLLocation alloc] initWithLatitude:state.droneLocation.latitude longitude:state.droneLocation.longitude];
    CLLocation *prevLocation = [[CLLocation alloc] initWithLatitude:state.homeLocation.latitude longitude:state.homeLocation.longitude];
    
    CLLocationDistance waypointDistance = [newLocation distanceFromLocation:prevLocation];
    self.distanceTextField.text = [NSString stringWithFormat:@"DISTANCE %0.2fft", [FSHelper toFeet:waypointDistance]];
    
    // set LABELS BELOW
    self.hSpeedTextField.text = [NSString stringWithFormat:@"H SPEED %0.1fm/s", (state.velocityX > state.velocityY) ? state.velocityX : state.velocityY];
    self.vSpeedTextField.text = [NSString stringWithFormat:@"V SPEED %0.1fm/s", state.velocityZ];
    self.headingTextField.text = [NSString stringWithFormat:@"HEADING %0.2f°", state.attitude.yaw];
//    self.pitchTextField.text = [NSString stringWithFormat:@"PITCH %0.2f°", state.attitude.pitch];
    self.heightTextField.text = [NSString stringWithFormat:@"HT %0.2fft\n", [FSHelper toFeet:state.altitude]];
}


-(void) groundStation:(id<DJIGroundStation>)gs didUpdateFlyingInformation:(DJIGroundStationFlyingInfo*)flyingInfo
{
    mCurrentDroneCoordinate = flyingInfo.droneLocation;
    
    NSString* ctrlModeText = [self onGroundStationControlModeChanged:flyingInfo.controlMode];
    [self onGroundStationGpsStatusChanged:flyingInfo.gpsStatus];
    
    NSMutableString* statsSting = [[NSMutableString alloc] init];
    [statsSting appendFormat:@"Mode: %@\n", ctrlModeText];
    [statsSting appendFormat:@"GPS: %d\n", flyingInfo.satelliteCount];

//    self.gpsStrengthTextField.text = [NSString stringWithFormat:@"%d FLY", flyingInfo.satelliteCount];

    
//    [statsSting appendFormat:@"Drone: {%0.6f, %0.6f}\n", flyingInfo.droneLocation.latitude, flyingInfo.droneLocation.longitude];
//    [statsSting appendFormat:@"Home: {%0.6f, %0.6f}\n", flyingInfo.homeLocation.latitude, flyingInfo.homeLocation.longitude];
//    
//    [statsSting appendFormat:@"TargetWPIndex: %d\n", flyingInfo.targetWaypointIndex];
//    [statsSting appendFormat:@"Pitch:%0.1f Roll:%0.1f Yaw:%0.1f\n", flyingInfo.attitude.pitch, flyingInfo.attitude.roll, flyingInfo.attitude.yaw];
//    [statsSting appendFormat:@"Vx: %0.1f m/s\n", flyingInfo.velocityX];
//    [statsSting appendFormat:@"Vy: %0.1f m/s\n", flyingInfo.velocityY];
//
//    [statsSting appendFormat:@"Vz: %0.1f m/s\n", flyingInfo.velocityZ];
//    [statsSting appendFormat:@"Alt: %f m\n", flyingInfo.altitude];
//    
//    [statsSting appendFormat:@"Status:%d", (int)flyingInfo.droneStatus];
//    self.statusLabelLeft.text = statsSting;
}

//-(void) groundStation:(id<DJIGroundStation>)gs didUploadWaypointMissionWithProgress:(uint8_t)progress
//{
//    NSString* message = [NSString stringWithFormat:@"Mission Uploading:%d%%", progress];
//    if (self.mProgressAlertView == nil) {
//        self.mProgressAlertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
//        [self.mProgressAlertView show];
//    }
//    else
//    {
//        [self.mProgressAlertView setMessage:message];
//    }
//    
//    if (progress == 100) {
//        [self.mProgressAlertView dismissWithClickedButtonIndex:0 animated:YES];
//        self.mProgressAlertView = nil;
//    }
//}
//
//-(void) groundStation:(id<DJIGroundStation>)gs didDownloadWaypointMissionWithProgress:(uint8_t)progress
//{
//    NSString* message = [NSString stringWithFormat:@"Mission Downloading:%d%%", progress];
//    if (self.mProgressAlertView == nil) {
//        self.mProgressAlertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
//        [self.mProgressAlertView show];
//    }
//    else
//    {
//        [self.mProgressAlertView setMessage:message];
//    }
//    
//    if (progress == 100) {
//        [self.mProgressAlertView dismissWithClickedButtonIndex:0 animated:YES];
//        self.mProgressAlertView = nil;
//    }
//}
//
#pragma mark - DJIDroneDelegate

-(void) droneOnConnectionStatusChanged:(DJIConnectionStatus)status
{
    switch (status) {
        case ConnectionStartConnect:
            
            break;
        case ConnectionSucceeded:
        {
            [self startBatteryUpdate];
            _connectionStatusLabel.text = @"CONTROLLER CONNECTED";
            [Flurry logEvent:@"Drone Connected"];
            [self setStatusUIToStatus:MODE_RED];
            break;
        }
        case ConnectionFailed:
        {
            _connectionStatusLabel.text = @"CONNECTION FAILED";
            [self setStatusUIToStatus:MODE_RED];
            [Flurry logEvent:@"Drone Connect Failed"];
            break;
        }
        case ConnectionBroken:
        {
            _connectionStatusLabel.text = @"DISCONNECTED";
            [self setStatusUIToStatus:MODE_RED];
            [Flurry logEvent:@"Drone DisConnected"];
            break;
        }
        default:
            break;
    }
}

-(void) setRecordingButtonState:(BOOL)isRecord
{
    if (isRecord) {
        [self.recordVideoButton setImage:[UIImage imageNamed:@"RecordVideoOn"] forState:UIControlStateNormal];
        [Flurry logEvent:@"Record Video"];
    }
    else
    {
        [self.recordVideoButton setImage:[UIImage imageNamed:@"RecordVideo"] forState:UIControlStateNormal];
        [Flurry logEvent:@"Stop Video"];
    }
}

-(void) setCaptureButtonState:(BOOL)isStopCapture
{
    if (isStopCapture) {
        [self.capturePhotoButton setBackgroundColor:[UIColor whiteColor]];
    }
    else
    {
        [self.capturePhotoButton setBackgroundColor:[UIColor clearColor]];
    }
}


#pragma mark - DJICameraDelegate

-(void) camera:(DJICamera*)camera didReceivedVideoData:(uint8_t*)videoBuffer length:(int)length
{
    uint8_t* pBuffer = (uint8_t*)malloc(length);
    memcpy(pBuffer, videoBuffer, length);
    [[[VideoPreviewer instance] dataQueue] push:pBuffer length:length];
}

-(void) camera:(DJICamera*)camera didUpdateSystemState:(DJICameraSystemState*)systemState
{
    if (self.mCameraSystemState) {
        if (self.mCameraSystemState.isRecording != systemState.isRecording) {
            [self setRecordingButtonState:systemState.isRecording];
        }
        if ((self.mCameraSystemState.isTakingMultiCapture != systemState.isTakingMultiCapture) ||
            (self.mCameraSystemState.isTakingContinusCapture != systemState.isTakingContinusCapture)) {
            BOOL isStop = systemState.isTakingContinusCapture || systemState.isTakingMultiCapture;
            [self setCaptureButtonState:isStop];
        }
        
        if (systemState.isUSBMode) {
            [_drone.camera setCamerMode:CameraCameraMode withResultBlock:nil];
        }
        
        if (systemState.isRecording) {
            if (self.mCameraSystemState.currentRecordingTime != systemState.currentRecordingTime) {
                int minute = (systemState.currentRecordingTime % 3600) / 60;
                int second = (systemState.currentRecordingTime % 3600) % 60;
                self.videoRecordLabel.text = [NSString stringWithFormat:@"%02d:%02d", minute, second];
            }
        }
    }
    else
    {
        [self setRecordingButtonState:systemState.isRecording];
        BOOL isStop = systemState.isTakingContinusCapture || systemState.isTakingMultiCapture;
        [self setCaptureButtonState:isStop];
    }
    
    self.mCameraSystemState = systemState;
    
}


-(void) camera:(DJICamera *)camera didUpdatePlaybackState:(DJICameraPlaybackState *)playbackState
{
    if (self.mCameraSystemState.workMode == CameraWorkModePlayback) {
        self.mCameraPlaybackState = playbackState;
        [self updateUIWithPlaybackState:playbackState];
        self.prevMediaButton.hidden = NO;
        self.nextMediaButton.hidden = NO;
    } else {
        self.playVideoBtn.hidden = YES;
        self.prevMediaButton.hidden = YES;
        self.nextMediaButton.hidden = YES;
    }
}

- (void)updateUIWithPlaybackState:(DJICameraPlaybackState *)playbackState
{
    if (playbackState.playbackMode == SingleFilePreview) {
        if (playbackState.mediaFileType == MediaFileJPEG || playbackState.mediaFileType == MediaFileDNG) { //Photo Type
            [self.playVideoBtn setHidden:YES];
        }else if (playbackState.mediaFileType == MediaFileVIDEO) //Video Type
        {
            [self.playVideoBtn setHidden:NO];
        }
    }else if (playbackState.playbackMode == SingleVideoPlaybackStart){ //Playing Video
        [self.playVideoBtn setHidden:YES];
    }else if (playbackState.playbackMode == MultipleFilesPreview){
        [self.playVideoBtn setHidden:YES];
    }
}

-(void) camera:(DJICamera *)camera didGeneratedNewMedia:(DJIMedia *)newMedia
{
    NSLog(@"GenerateNewMedia:%@",newMedia.mediaURL);
}

// Video playback screen

- (IBAction)preVideoButtonPressed:(id)sender {
    [self.mInspireCamera singlePreviewPreviousPage];
}

- (IBAction)nextVideoButtonPressed:(id)sender {
    [self.mInspireCamera singlePreviewNextPage];
}

- (IBAction)goBackToFPVPressed:(id)sender {
    if (self.photoVideoSwitch.on) {
        [self setVideoMode:CameraWorkModeRecord];
    } else {
        [self setVideoMode:CameraWorkModeCapture];
    }

    [self setVideoMode:CameraWorkModeRecord];

    [[VideoPreviewer instance] setView:self.videoPreviewView];
    [self.view sendSubviewToBack:self.videoView];
}


#pragma mark Gesture

- (void)swipeLeftGestureAction:(UISwipeGestureRecognizer *)gesture
{
    [self.mInspireCamera singlePreviewNextPage];
}

- (void)swipeRightGestureAction:(UISwipeGestureRecognizer *)gesture
{
    [self.mInspireCamera singlePreviewPreviousPage];
}


#pragma mark Timers

-(void) startBatteryUpdate
{
    if (_readBatteryInfoTimer == nil) {
        _readBatteryInfoTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(onReadBatteryInfoTimerTicked:) userInfo:nil repeats:YES];
    }
}

-(void) stopBatterUpdate
{
    if (_readBatteryInfoTimer) {
        [_readBatteryInfoTimer invalidate];
        _readBatteryInfoTimer = nil;
    }
}

-(void) onReadBatteryInfoTimerTicked:(id)timer
{
    [_drone.smartBattery updateBatteryInfo:^(DJIError *error) {
        if (error.errorCode == ERR_Succeeded) {
            
            self.batteryLifeTextField.text = [NSString stringWithFormat:@"%ld%%",(long)_drone.smartBattery.remainPowerPercent];
        }
        else
        {
            NSLog(@"update BatteryInfo Failed");
        }
    }];
}


-(void) startMessageTimer
{
    if (_messageTimer == nil) {
        _messageTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(onMessageTimer:) userInfo:nil repeats:YES];
    }
}

-(void) stopMessageTimer
{
    if (_messageTimer) {
        [_messageTimer invalidate];
        _messageTimer = nil;
    }
}

-(void)stopAnimation {
    [self.view.layer removeAllAnimations];
}

- (void)onMessageTimer:(id)timer
{
    [self performSelectorOnMainThread:@selector(stopAnimation) withObject:nil waitUntilDone:YES];

    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.messageView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.view sendSubviewToBack:self.messageView];
        [self stopMessageTimer];
    }];
}


- (NSString *)flyingInfoString
{
    if (self.state.satelliteCount < 5) {
        return @" (NON-GPS)";
    }
    
    return @"";
}

#pragma mark DJIRemoteControllerDelegate


-(void) remoteController:(DJIRemoteController*)rc didUpdateGpsData:(DJIRCGPSData)gpsData
{
//    self.gpsStrengthTextField.text = [NSString stringWithFormat:@"%d REM", gpsData.mSatelliteCount];
}

-(void) remoteController:(DJIRemoteController *)rc didUpdateBatteryState:(DJIRCBatteryInfo)batteryInfo
{
    self.mRCBatteryInfo = batteryInfo;
}


-(void) imageTransmitter:(DJIImageTransmitter*)transmitter didUpdateRadioSignalQuality:(DJIImageTransmitterRadioSignalQuality)quality
{
    if (quality.mUpLink) {
        self.controllerStrengthTextField.text = [NSString stringWithFormat:@"%d%%", quality.mPercent];
    }
    else
    {
        self.cameraStrengthTextField.text = [NSString stringWithFormat:@"%d%%",quality.mPercent];
    }
    
    if (quality.mPercent > 0) {
        if (quality.mPercent == 100) {
            if (self.state != nil) {
                if ((!self.state.isIMUPreheating)&&(!self.state.isCompassError)) {
                    _connectionStatusLabel.text = [NSString stringWithFormat:@"SAFE TO FLY%@", [self flyingInfoString]];
                    [self setStatusUIToStatus:MODE_GREEN];
                }
            }
        } else {
            if (self.state != nil) {
                if ((!self.state.isIMUPreheating)&&(!self.state.isCompassError)) {
                    _connectionStatusLabel.text = [NSString stringWithFormat:@"SAFE TO FLY%@", [self flyingInfoString]];
                    [self setStatusUIToStatus:MODE_YELLOW];
                }
            }
        }
    } else {
        _connectionStatusLabel.text = @"DRONE DISCONNECTED";
        [self setStatusUIToStatus:MODE_RED];
        self.cameraStrengthTextField.text = @"N/A";
        self.controllerStrengthTextField.text = @"N/A";
    }
}


#pragma mark SourceForPageViewControllerDelegate

- (void)sourceForPageViewController:(SourceForPageViewController *)pageViewController didPressButtonIndex:(NSInteger)buttonIndex withCompletion:(PageDelegateBlock)completion
{
    if (completion) {
        completion(YES);
        return;
    }

    
    WeakSelf weakSelf = self;
    
    [self.mInspireMainController startTakeoffWithResult:^(DJIError *error) {
        if (error.errorCode == ERR_Succeeded) {
            [weakSelf performSelector:@selector(launchMission) withObject:self afterDelay:10.0];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (SIMULATION) {
                    [weakSelf setPlay:NO];
                    [self launchMission];
                } else {
                    ShowResult(@"Drone take off failed: %@", error.errorDescription);
                    [weakSelf setPlay:YES];
                }
            });
        }
    }];

    
}



- (void)launchMission
{
    WeakSelf weakSelf = self;
    
    [self.navigationManager enterNavigationModeWithResult:^(DJIError *error) {
        if (error.errorCode == ERR_Succeeded) {
            StrongSelf strongSelf = weakSelf;
            
            if (strongSelf) {
                [strongSelf setPlay:NO];
                [strongSelf setTimerMessage:[NSString stringWithFormat:@"Auto Pilot Enabled"]];
                [Flurry logEvent:@"Controller Communication Success"];
                [strongSelf uploadTaskWithCompletion:^(BOOL uploadTaskDone) {
                    if(uploadTaskDone) {
                        [strongSelf uploadAndStartMission];
                    }
                }];
                [Flurry logEvent:@"Task Uploaded"];
            }
        } else {
            if (SIMULATION) {
                [weakSelf setPlay:NO];
                [weakSelf setTimerMessage:[NSString stringWithFormat:@"Auto Pilot Enabled"]];
            } else {
                ShowResult(@"Enter Navigation Mode:%@", error.errorDescription);
                [Flurry logEvent:@"Cannot Communicate with the Controller"];
            }
        }
    }];
}

- (void)uploadAndStartMission {
    WeakSelf weakSelf = self;
    self.startDate = [NSDate date];
    if (self.totalMissionTimeInfoTimer != nil) {
        [self.totalMissionTimeInfoTimer invalidate];
        self.totalMissionTimeInfoTimer = nil;
    }
    self.totalMissionTimeInfoTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(onTick:) userInfo:nil repeats:YES];
    
    [Flurry logEvent:@"Launch Mission!!!!!"];

    [self.waypointMission startMissionWithResult:^(DJIError *error) {
        StrongSelf strongSelf = weakSelf;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (strongSelf) {
                [self setTimerMessage:[NSString stringWithFormat:@"Launching Mission...%@", error.errorDescription]];
                
                if (error.errorCode == ERR_Succeeded) {
                    [strongSelf setPlay:NO];
                } else {
                    [strongSelf setPlay:YES];
                }
            }
        });
    }];
    
}

#pragma mark - DJIGimbalDelegate

-(void) gimbalController:(DJIGimbal*)controller didGimbalError:(DJIGimbalError)error
{
    if (error == GimbalClamped) {
        NSLog(@"Gimbal Clamped");
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Gimbal Clamped" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    if (error == GimbalErrorNone) {
        NSLog(@"Gimbal Error None");
        
    }
    if (error == GimbalMotorAbnormal) {
        NSLog(@"Gimbal Motor Abnormal");
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Gimbal Motor Abnormal" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

-(void) gimbalController:(DJIGimbal *)controller didUpdateGimbalState:(DJIGimbalState*)gimbalState
{
    
//    NSString* atti = [NSString stringWithFormat:@"Pitch:%0.1f\tRoll:%0.1f\tYaw:%0.1f", gimbalState.attitude.pitch, gimbalState.attitude.roll, gimbalState.attitude.yaw];
    self.pitchTextField.text = [NSString stringWithFormat:@"PITCH %0.2f°", gimbalState.attitude.pitch];
    
}

@end

