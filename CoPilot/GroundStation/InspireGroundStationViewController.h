//
//  GroundStationTestViewController.h
//  DJIVisionSDK
//
//  Created by Ares on 14-7-16.
//  Copyright (c) 2014年 DJI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DJISDK/DJISDK.h>
#import "DJILogerViewController.h"
#import "DJIDemoHelper.h"

typedef NS_ENUM(NSInteger, PreFlightMode) {
    TYPE_NONE = 0, ///< Undefined
    TYPE_WEATHER,
    TYPE_LAUNCH,
};


typedef NS_ENUM(NSInteger, FlightMode) {
    FLIGHTMODE_AUTO = 0,
    FLIGHTMODE_PITCH,
    FLIGHTMODE_CAMERA,
    FLIGHTMODE_MANUAL
};

typedef NS_ENUM(NSInteger, StatusMode) {
    MODE_RED,
    MODE_YELLOW,
    MODE_GREEN
};

@interface InspireGroundStationViewController : DJILogerViewController <DJIDroneDelegate, GroundStationDelegate, DJIMainControllerDelegate, DJINavigationDelegate, DJICameraDelegate, DJIGimbalDelegate>
{

    DJIWaypointMissionStatus* _lastWaypointStatus;
    
    BOOL _isPOIMissionStarted;
    BOOL _isPOIMissionPaused;
    
    CLLocationCoordinate2D mCurrentDroneCoordinate;
    
}

@property(nonatomic, strong) UISwipeGestureRecognizer *swipeLeftGesture;
@property(nonatomic, strong) UISwipeGestureRecognizer *swipeRightGesture;
@property(nonatomic, strong) UISwipeGestureRecognizer *swipeUpGesture;
@property(nonatomic, strong) UISwipeGestureRecognizer *swipeDownGesture;


@property(nonatomic, strong) UIAlertView* mProgressAlertView;
@property(nonatomic, strong) NSDictionary* flightModeDict;

// New 2.3 SDK additions
@property(nonatomic, weak) NSObject<DJINavigation>* navigationManager;
@property(nonatomic, weak) NSObject<DJIWaypointMission>* waypointMission;
//

@property(nonatomic, strong) DJIInspireMainController* mInspireMainController;
@property(nonatomic, strong) DJIDrone* drone;
@property(nonatomic, strong) DJIPhantom3AdvancedGimbal* mGimbal;
@property(nonatomic, strong) UILabel* statusLabelLeft;
@property(nonatomic, strong) IBOutlet UIScrollView* scrollViewLeft;


-(IBAction) onStartTaskButtonClicked:(id)sender;

-(IBAction) onStopTaskButtonClicked:(id)sender;

-(IBAction) onPauseTaskButtonClicked:(id)sender;

-(IBAction) onBackButtonClicked:(id)sender;

@end
