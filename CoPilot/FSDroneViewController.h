//
//  FSDroneViewController.h
//  CoPilot
//
//  Created by Pronob Ashwin on 6/4/15.
//  Copyright (c) 2015 FreeSkies. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ImageDataCompletionBlock)(NSData *imageData);

typedef NS_ENUM(NSInteger, HelpMode) {
    HELPSCREEN_1,
    HELPSCREEN_2,
    HELPSCREEN_DISMISSED
};

@interface FSDroneViewController : UIViewController

@end
