//
//  FSWaypointModelObject.m
//  copilot
//
//  Created by Pronob Ashwin on 6/26/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSWaypointModelObject.h"
#import "FSHelper.h"

@implementation FSWaypointModelObject


- (instancetype)initWithCamera:(MKMapCamera *)camera horizontalVelocity:(CGFloat)velocity stayTime:(NSInteger)stayTime looklingAtCoordinate:(CLLocationCoordinate2D)lookingAtCoordinate forMapImageData:(NSData *)imageData
{
    self = [super init];
    if (self) {
        self.mapCamera = camera;
        
        CLLocationCoordinate2D droneCoordinates = [FSHelper getDroneCoordinateForMapCamera:camera lookingAtCenterCoordinate:lookingAtCoordinate];
        CGFloat droneAltitude = [FSHelper getDroneAltitudeForMapCameraAltitude:camera.altitude];
        CGFloat droneHeading = [FSHelper convertToDJIHeading:camera.heading];
        CGFloat dronePitch = [FSHelper convertToDJIAngle:camera.pitch];
        self.waypoint = [self initializeWaypointWith:droneCoordinates altitude:droneAltitude heading:droneHeading horizontalVelocity:velocity withGimbalPitchAngle:dronePitch];

        self.mapImageData = imageData;
        self.lookingAtCoordinate = lookingAtCoordinate;
        self.cameraPitch = [FSHelper convertToDJIAngle:camera.pitch];
        
    }
 
    return self;
}


- (DJIWaypoint *)initializeWaypointWith:(CLLocationCoordinate2D)droneCoordinates altitude:(CGFloat)altitude heading:(CGFloat)heading horizontalVelocity:(CGFloat)horizontalVelocity withGimbalPitchAngle:(CGFloat)cameraPitch
{
    DJIWaypoint * waypoint = [[DJIWaypoint alloc] initWithCoordinate:droneCoordinates];
    waypoint.altitude = altitude;
    waypoint.heading = heading;
//    waypoint.horizontalVelocity = horizontalVelocity;
    waypoint.actionTimeout = 10;
    
    DJIWaypointAction* action = [[DJIWaypointAction alloc] initWithActionType:DJIWaypointActionRotateGimbalPitch param:cameraPitch];
    [waypoint addAction:action];
    
    return waypoint;
    
}

- (CGFloat)getPitchAction
{
    CGFloat pitch = 0.0;
    
    DJIWaypoint *waypoint = self.waypoint;
    
    for (DJIWaypointAction *action in waypoint.waypointActions) {
        if (action.actionType == DJIWaypointActionRotateGimbalPitch) {
            pitch = (CGFloat)action.actionParam;
            break;
        }
    }
    
    return pitch;
}

#pragma mark NSCoding


- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_pathName forKey:@"pathName"];
    
    [encoder encodeObject:_mapCamera forKey:@"mapCamera"];
    [encoder encodeFloat:_cameraPitch forKey:@"cameraPitch"];
    [encoder encodeObject:_alphaLabel forKey:@"alphaLabel"];
    [encoder encodeDouble:_waypoint.coordinate.latitude forKey:@"waypoint.coordinate.latitude"];
    [encoder encodeDouble:_waypoint.coordinate.longitude forKey:@"waypoint.coordinate.longitude"];
    [encoder encodeFloat:_waypoint.altitude forKey:@"waypoint.altitude"];
    [encoder encodeFloat:_waypoint.heading forKey:@"waypoint.heading"];
//    [encoder encodeFloat:_waypoint.horizontalVelocity forKey:@"waypoint.horizontalVelocity"];
    
    if (self.waypoint.waypointActions.count > 0) {
        for (DJIWaypointAction *action in self.waypoint.waypointActions) {
            if (action.actionType == DJIWaypointActionRotateGimbalPitch) {
                [encoder encodeInt:action.actionParam forKey:@"waypoint.gimbalPitch"];
            }
        }
    }
    
    [encoder encodeObject:_mapImageData forKey:@"mapImageData"];
    [encoder encodeDouble:_lookingAtCoordinate.latitude forKey:@"lookingAtCoordinate.coordinate.latitude"];
    [encoder encodeDouble:_lookingAtCoordinate.longitude forKey:@"lookingAtCoordinate.coordinate.longitude"];
    [encoder encodeFloat:_timeFromPreviousWaypoint forKey:@"timeFromPreviousWaypoint"];
    [encoder encodeDouble:_distanceFromHomeWaypoint forKey:@"distanceFromHomeWaypoint"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super init]))
    {
        self.pathName = [decoder decodeObjectForKey:@"pathName"];
        self.mapCamera = [decoder decodeObjectForKey:@"mapCamera"];
        self.cameraPitch = [decoder decodeFloatForKey:@"cameraPitch"];
        self.alphaLabel = [decoder decodeObjectForKey:@"alphaLabel"];
        CLLocationDegrees latitude = [decoder decodeDoubleForKey:@"waypoint.coordinate.latitude"];
        CLLocationDegrees longitude = [decoder decodeDoubleForKey:@"waypoint.coordinate.longitude"];
        CLLocationCoordinate2D droneCoordinate = CLLocationCoordinate2DMake(latitude, longitude);
        CGFloat droneAltitude = [decoder decodeFloatForKey:@"waypoint.altitude"];
        CGFloat droneHeading = [decoder decodeFloatForKey:@"waypoint.heading"];
//        CGFloat horizontalVelocity = [decoder decodeFloatForKey:@"waypoint.horizontalVelocity"];
        CGFloat dronePitch = [decoder decodeIntForKey:@"waypoint.gimbalPitch"];
        self.waypoint = [self initializeWaypointWith:droneCoordinate altitude:droneAltitude heading:droneHeading horizontalVelocity:0.0 withGimbalPitchAngle:dronePitch];

        self.mapImageData = [decoder decodeObjectForKey:@"mapImageData"];
        CLLocationDegrees clatitude = [decoder decodeDoubleForKey:@"lookingAtCoordinate.coordinate.latitude"];
        CLLocationDegrees clongitude = [decoder decodeDoubleForKey:@"lookingAtCoordinate.coordinate.longitude"];
        self.lookingAtCoordinate = CLLocationCoordinate2DMake(clatitude, clongitude);
        self.timeFromPreviousWaypoint = [decoder decodeFloatForKey:@"timeFromPreviousWaypoint"];
        self.distanceFromHomeWaypoint = [decoder decodeFloatForKey:@"distanceFromHomeWaypoint"];
    }
    return self;
}


@end
