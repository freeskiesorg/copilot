//
//  FSPathModelObject.m
//  copilot
//
//  Created by Pronob Ashwin on 6/28/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSPathModelObject.h"
#import "FSHelper.h"
#import <DJISDK/DJISDK.h>

@implementation FSPathModelObject


- (instancetype)initPathWithName:(NSString *)pathName
{
    self = [super init];
    if (self) {
        self.pathName = pathName;
        self.waypointArray = [NSMutableArray array];
    }
    
    return self;
}

- (void)addWaypointLocationForMapCamera:(MKMapCamera *)mapCamera droneAltitude:(CGFloat)droneAltitude dronePitch:(CGFloat)dronePitch withHorizontalVelocity:(CGFloat)velocity stayTime:(NSInteger)stayTime lookingAtCoordinate:(CLLocationCoordinate2D)lookingAtCoordinate forMapImageData:(NSData *)imageData andAnnotation:(FSWaypointAnnotation *)waypointAnnotation
{
    FSWaypointModelObject *waypointObject = [[FSWaypointModelObject alloc] init];
    waypointObject.mapCamera = mapCamera;
    
    CLLocationCoordinate2D droneCoordinates = [FSHelper getDroneCoordinateForMapCamera:mapCamera lookingAtCenterCoordinate:lookingAtCoordinate];
    CGFloat droneHeading = [FSHelper convertToDJIHeading:mapCamera.heading];
    waypointObject.waypoint = [waypointObject initializeWaypointWith:droneCoordinates altitude:droneAltitude heading:droneHeading horizontalVelocity:velocity withGimbalPitchAngle:dronePitch];

    waypointObject.mapImageData = imageData;
    waypointObject.lookingAtCoordinate = lookingAtCoordinate;
    waypointObject.cameraPitch = dronePitch; // above init dups pitch
    
    NSString *alphaLabel = [FSHelper getAlphaCharacter:(int)self.waypointArray.count];
    
    waypointAnnotation.waypointLabel = alphaLabel;
    waypointObject.waypointAnnotation = waypointAnnotation;
    waypointObject.alphaLabel = alphaLabel;
    
    [self addRotationActionForWaypoint:waypointObject atIndex:self.waypointArray.count];
    [self.waypointArray addObject:waypointObject];
}


- (void)updateWaypointLocationForMapCamera:(MKMapCamera *)mapCamera droneAltitude:(CGFloat)droneAltitude dronePitch:(CGFloat)dronePitch withHorizontalVelocity:(CGFloat)velocity stayTime:(NSInteger)stayTime lookingAtCoordinate:(CLLocationCoordinate2D)lookingAtCoordinate forMapImageData:(NSData *)imageData forIndex:(NSInteger)index andAnnotation:(FSWaypointAnnotation *)updatedWaypointAnnotation
{
    FSWaypointModelObject *waypointObject = [[FSWaypointModelObject alloc] init];
    waypointObject.mapCamera = mapCamera;

    CLLocationCoordinate2D droneCoordinates = [FSHelper getDroneCoordinateForMapCamera:mapCamera lookingAtCenterCoordinate:lookingAtCoordinate];
    CGFloat droneHeading = [FSHelper convertToDJIHeading:mapCamera.heading];
    waypointObject.waypoint = [waypointObject initializeWaypointWith:droneCoordinates altitude:droneAltitude heading:droneHeading horizontalVelocity:velocity withGimbalPitchAngle:dronePitch];
    
    waypointObject.mapImageData = imageData;
    waypointObject.lookingAtCoordinate = lookingAtCoordinate;
    waypointObject.cameraPitch = dronePitch;
    
    NSString *alphaLabel = [FSHelper getAlphaCharacter:(int)index];

    updatedWaypointAnnotation.waypointLabel = alphaLabel;
    waypointObject.waypointAnnotation = updatedWaypointAnnotation;
    waypointObject.alphaLabel = alphaLabel;
    
    [self addRotationActionForWaypoint:waypointObject atIndex:index];
    [self.waypointArray replaceObjectAtIndex:index withObject:waypointObject];
}

- (FSWaypointAnnotation *)getAnnotationForChosenWaypointIndex:(NSInteger)index;
{
    FSWaypointModelObject *waypointObject = [self.waypointArray objectAtIndex:index];
    
    return waypointObject.waypointAnnotation;
}


- (void)addRotationActionForWaypoint:(FSWaypointModelObject *)waypointObject atIndex:(NSInteger)index
{
    if (index == 0) {
        return;
    }
    
    FSWaypointModelObject *prevWaypointObject = [self.waypointArray objectAtIndex:index-1];
    
    CGFloat prevHeadingAngle = prevWaypointObject.waypoint.heading + 180; // angle is -180 to 180 so convert to 360 coordinate space
    CGFloat currentHeadingAngle = waypointObject.waypoint.heading + 180; // angle is -180 to 180 so convert to 360 coordinate space
    
    
    if (currentHeadingAngle > prevHeadingAngle) {
        CGFloat angleDiff = currentHeadingAngle - prevHeadingAngle;
        if (angleDiff > 180) {
            prevWaypointObject.waypoint.turnMode = DJIWaypointTurnCounterClockwise;
        } else {
            prevWaypointObject.waypoint.turnMode = DJIWaypointTurnClockwise;
        }
    } else {
        CGFloat angleDiff = prevHeadingAngle - currentHeadingAngle;
        if (angleDiff > 180) {
            prevWaypointObject.waypoint.turnMode = DJIWaypointTurnClockwise;
        } else {
            prevWaypointObject.waypoint.turnMode = DJIWaypointTurnCounterClockwise;
        }
    }
    
    
}


- (void)reshuffleAnnotations
{
    int count = 0;
    for (FSWaypointModelObject *waypointObject in self.waypointArray) {
        NSString *alphaLabel = [FSHelper getAlphaCharacter:count];
        waypointObject.alphaLabel = alphaLabel;
        waypointObject.waypointAnnotation.waypointLabel = alphaLabel;
        count ++;
    }
}

#pragma mark NSCoding


- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_pathName forKey:@"pathName"];
    
    [encoder encodeObject:_waypointArray forKey:@"waypointArray"];
    [encoder encodeObject:_standardMapImageData forKey:@"standardMapImageData"];
    [encoder encodeObject:_hybridMapImageData forKey:@"hybridMapImageData"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super init]))
    {
        self.pathName = [decoder decodeObjectForKey:@"pathName"];
        self.waypointArray = [decoder decodeObjectForKey:@"waypointArray"];
        self.standardMapImageData = [decoder decodeObjectForKey:@"standardMapImageData"];
        self.hybridMapImageData = [decoder decodeObjectForKey:@"hybridMapImageData"];
        
        int count = 0;
        for (FSWaypointModelObject *modelObject in self.waypointArray) {
            CLLocationCoordinate2D droneCoordinates = [FSHelper getDroneCoordinateForMapCamera:modelObject.mapCamera lookingAtCenterCoordinate:modelObject.lookingAtCoordinate];

            FSWaypointAnnotation *annotation = [[FSWaypointAnnotation alloc] initWithCoordinate:droneCoordinates andWaypointLabel:[FSHelper getAlphaCharacter:count]];
            
            count++;
            [annotation setTitle:[NSString stringWithFormat:@"Altitude: %f", modelObject.waypoint.altitude]]; //You can set the subtitle too
            
            modelObject.waypointAnnotation = annotation;
        }
        
    }
    return self;
}

- (MKCoordinateRegion) getSpanRegionForWaypoints
{
    CLLocationDegrees minLatitude = 90.0;
    CLLocationDegrees minLongitude = 180.0;
    CLLocationDegrees maxLatitude = -90.0;
    CLLocationDegrees maxLongitude = -180.0;
    
    for (FSWaypointModelObject *waypointModel in self.waypointArray) {
        if (minLatitude > waypointModel.waypoint.coordinate.latitude) {
            minLatitude = waypointModel.waypoint.coordinate.latitude;
        }
        if (minLongitude > waypointModel.waypoint.coordinate.longitude) {
            minLongitude = waypointModel.waypoint.coordinate.longitude;
        }
        if (maxLatitude < waypointModel.waypoint.coordinate.latitude) {
            maxLatitude = waypointModel.waypoint.coordinate.latitude;
        }
        if (maxLongitude < waypointModel.waypoint.coordinate.longitude) {
            maxLongitude = waypointModel.waypoint.coordinate.longitude;
        }
    }

    double miles = 0.2;
    double scalingFactor = ABS( (cos(2 * M_PI * maxLatitude / 360.0) ));
    

    CLLocationDegrees latitudeDelta =  maxLatitude - minLatitude;
    CLLocationDegrees longitudeDelta =  maxLongitude - minLongitude;
    
    latitudeDelta += miles/69.0;
    longitudeDelta += miles/(scalingFactor * 69.0);
    
    MKCoordinateSpan span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta);
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(((maxLatitude + minLatitude)/2), ((maxLongitude + minLongitude)/2));
    MKCoordinateRegion region = MKCoordinateRegionMake(centerCoordinate, span);
    
    return region;
}

@end
