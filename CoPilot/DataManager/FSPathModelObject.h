//
//  FSPathModelObject.h
//  copilot
//
//  Created by Pronob Ashwin on 6/28/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FSWaypointModelObject.h"
#import <MapKit/MapKit.h>

@interface FSPathModelObject : NSObject <NSCoding>

@property (copy, nonatomic) NSString *pathName;
@property (strong, nonatomic) NSMutableArray *waypointArray;
@property (strong, nonatomic) NSData *standardMapImageData;
@property (strong, nonatomic) NSData *hybridMapImageData;


- (instancetype)initPathWithName:(NSString *)pathName;
- (void)addWaypointLocationForMapCamera:(MKMapCamera *)mapCamera droneAltitude:(CGFloat)droneAltitude dronePitch:(CGFloat)dronePitch withHorizontalVelocity:(CGFloat)velocity stayTime:(NSInteger)stayTime lookingAtCoordinate:(CLLocationCoordinate2D)lookingAtCoordinate forMapImageData:(NSData *)imageData andAnnotation:(FSWaypointAnnotation *)waypointAnnotation;

- (void)updateWaypointLocationForMapCamera:(MKMapCamera *)mapCamera droneAltitude:(CGFloat)droneAltitude dronePitch:(CGFloat)dronePitch withHorizontalVelocity:(CGFloat)velocity stayTime:(NSInteger)stayTime lookingAtCoordinate:(CLLocationCoordinate2D)lookingAtCoordinate forMapImageData:(NSData *)imageData forIndex:(NSInteger)index andAnnotation:(FSWaypointAnnotation *)updatedWaypointAnnotation;

- (FSWaypointAnnotation *)getAnnotationForChosenWaypointIndex:(NSInteger)index;
- (void)reshuffleAnnotations;
- (MKCoordinateRegion) getSpanRegionForWaypoints;

@end
