//
//  FSWaypointModelObject.h
//  copilot
//
//  Created by Pronob Ashwin on 6/26/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DJISDK/DJISDK.h>
#import <MapKit/MapKit.h>
#import "FSWaypointAnnotation.h"

@interface FSWaypointModelObject : NSObject <NSCoding>

@property (nonatomic, strong) NSString *pathName;
@property (nonatomic, strong) NSString *alphaLabel;
@property (nonatomic, strong) MKMapCamera *mapCamera;
@property (nonatomic) CGFloat cameraPitch;
@property (nonatomic, strong) DJIWaypoint *waypoint;
@property (nonatomic, strong) NSData *mapImageData;
@property (strong, nonatomic) FSWaypointAnnotation *waypointAnnotation;
@property (nonatomic) CLLocationCoordinate2D lookingAtCoordinate;
@property (nonatomic) float timeFromPreviousWaypoint;
@property (nonatomic) CLLocationDistance distanceFromHomeWaypoint;


- (instancetype)initWithCamera:(MKMapCamera *)camera horizontalVelocity:(CGFloat)velocity stayTime:(NSInteger)stayTime looklingAtCoordinate:(CLLocationCoordinate2D)lookingAtCoordinate forMapImageData:(NSData *)imageData;
- (DJIWaypoint *)initializeWaypointWith:(CLLocationCoordinate2D)droneCoordinates altitude:(CGFloat)altitude heading:(CGFloat)heading horizontalVelocity:(CGFloat)horizontalVelocity withGimbalPitchAngle:(CGFloat)cameraPitch;
- (CGFloat)getPitchAction;
@end
