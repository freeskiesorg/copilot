//
//  FSDataManager.h
//  copilot
//
//  Created by Pronob Ashwin on 6/26/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "FSPathModelObject.h"

@interface FSDataManager : NSObject

+ (instancetype)sharedManager;

@property (nonatomic, strong) NSMutableArray *pathsArray;
@property (nonatomic, strong) FSPathModelObject *currentPathModel;

- (FSPathModelObject *)createPathObject;
- (FSPathModelObject *)createPathWithName:(NSString *)pathName;
- (void)savePath:(FSPathModelObject *)pathObject;
- (void)removePath:(FSPathModelObject *)pathObject;
- (void)saveCurrentPathWithPathName:(NSString *)pathName;

- (BOOL)isFirstLaunch;

@end
