//
//  FSDataManager.m
//  copilot
//
//  Created by Pronob Ashwin on 6/26/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSDataManager.h"
#import "FSWaypointModelObject.h"
#import "FSPathModelObject.h"

@implementation FSDataManager


+ (instancetype)sharedManager
{
    static dispatch_once_t onceToken;
    static id sharedManager;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self loadPathsFromStore];
        if (self.pathsArray == nil) {
            self.pathsArray = [NSMutableArray array];
        }
    }
    return self;
}

- (BOOL)isFirstLaunch
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"firstRun"])
    {
        return NO;
    }
    
    [defaults setObject:[NSDate date] forKey:@"firstRun"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return YES;
}

- (FSPathModelObject *)createPathObject
{
    FSPathModelObject *pathObject = [[FSPathModelObject alloc] initPathWithName:@""];
    self.currentPathModel = pathObject;
    return pathObject;
}


- (FSPathModelObject *)createPathWithName:(NSString *)pathName
{
    FSPathModelObject *pathObject = [[FSPathModelObject alloc] initPathWithName:pathName];
    self.currentPathModel = pathObject;
    return pathObject;
}


- (void)loadPathsFromStore
{
    // First check to see if data exists from a previous load. Else download the data from the server
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [currentDefaults objectForKey:@"pathsArray"];
    if (dataRepresentingSavedArray != nil)
    {
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        if (oldSavedArray != nil) {
            self.pathsArray = [[NSMutableArray alloc] initWithArray:oldSavedArray];
            return;
        }
    }

}

- (void)saveCurrentPathWithPathName:(NSString *)pathName
{
    self.currentPathModel.pathName = pathName;
    [self savePath:self.currentPathModel];
}

- (void)savePath:(FSPathModelObject *)pathObject
{
    if ([self.pathsArray containsObject:pathObject]) {
        [self persistPath];
        return;
    }
    [self.pathsArray addObject:pathObject];

    [self persistPath];
}

- (void)removePath:(FSPathModelObject *)pathObject
{
    [self.pathsArray removeObject:pathObject];
    
    [self persistPath];
}

- (void)persistPath
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:self.pathsArray] forKey:@"pathsArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

@end
