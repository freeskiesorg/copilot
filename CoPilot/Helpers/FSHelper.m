//
//  FSHelper.m
//  CoPilot
//
//  Created by Pronob Ashwin on 6/29/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSHelper.h"
#include <math.h>
#import <MapKit/MapKit.h>
#import "FSGlobalDefines.h"
#import "FSDataManager.h"


@implementation FSHelper


+ (CLLocationCoordinate2D) getDroneCoordinateForMapCamera:(MKMapCamera *)mapCamera lookingAtCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
{
    CLLocationCoordinate2D locationCoordinate = [FSHelper getDroneCoordinateForAltitude:mapCamera.altitude pitch:mapCamera.pitch heading:mapCamera.heading lookingAtCenterCoordinate:centerCoordinate];
    return locationCoordinate;
}

+ (CLLocationCoordinate2D) getDroneCoordinateForAltitude:(CLLocationDistance)altitude pitch:(CGFloat)pitch heading:(CLLocationDirection)heading lookingAtCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
{
    CLLocationDistance droneAltitude = [FSHelper getDroneAltitudeForMapCameraAltitude:altitude];
    
    CLLocationDistance distanceFromLookingAtCoord =  [FSHelper getDistanceFromLookingAtCoordinateForAltitude:droneAltitude andPitch:pitch];
    
    double distance;
    distance = -(distanceFromLookingAtCoord/1000);
    
    CLLocationCoordinate2D droneCoordinate = [FSHelper coordinateFromCoord:centerCoordinate atDistanceKm:distance atBearingDegrees:heading];
    
    return droneCoordinate;
}


+ (CLLocationCoordinate2D) getCameraCoordinateForCameraAltitude:(CLLocationDistance)cameraAltitude lookingAtCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate forMapCamera:(MKMapCamera *)mapCamera
{
    CLLocationCoordinate2D coordinate = [FSHelper getCameraCoordinateForCameraAltitude:cameraAltitude pitch:mapCamera.pitch heading:mapCamera.heading lookingAtCenterCoordinate:centerCoordinate];
    return coordinate;
}

+ (CLLocationCoordinate2D) getCameraCoordinateForCameraAltitude:(CLLocationDistance)cameraAltitude pitch:(CGFloat)pitch heading:(CLLocationDirection)heading lookingAtCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
{
    CLLocationDistance distanceFromLookingAtCoord =  [FSHelper getDistanceFromLookingAtCoordinateForAltitude:cameraAltitude andPitch:pitch];
    
    double distance;
    distance = -(distanceFromLookingAtCoord/1000);
    distance *= 0.70;
    
    CLLocationCoordinate2D destinationCoordinate = [FSHelper coordinateFromCoord:centerCoordinate atDistanceKm:distance atBearingDegrees:heading];
    
    return destinationCoordinate;
}

+ (CLLocationDistance)getDroneAltitudeForMapCameraAltitude:(CLLocationDistance)mapCameraAltitude
{
    CLLocationDistance distance = (DRONE_CAMERA_COMPENSATION_FACTOR *mapCameraAltitude);
    return distance;
}

+ (CLLocationCoordinate2D) getMapCameraCoordinateForDroneAltitude:(CLLocationDistance)droneAltitude pitch:(CGFloat)pitch withBearing:(CLLocationDirection)bearing andDroneCoordinate:(CLLocationCoordinate2D)droneCoordinate
{
    CLLocationDistance distanceFromLookingAtCoord =  [FSHelper getDistanceFromLookingAtCoordinateForAltitude:droneAltitude andPitch:pitch];
    
    double distance;
    distance = -(distanceFromLookingAtCoord/1000);
    
    CLLocationCoordinate2D mapCoordinate = [FSHelper coordinateFromCoord:droneCoordinate atDistanceKm:distance/2 atBearingDegrees:bearing];
    
    return mapCoordinate;
}

+ (CLLocationCoordinate2D)coordinateFromCoord:(CLLocationCoordinate2D)fromCoord
                                 atDistanceKm:(double)distanceKm
                             atBearingDegrees:(double)bearingDegrees
{
    double distanceRadians = distanceKm / 6371.0;
    //6,371 = Earth's radius in km
    double bearingRadians = [self radiansFromDegrees:bearingDegrees];
    double fromLatRadians = [self radiansFromDegrees:fromCoord.latitude];
    double fromLonRadians = [self radiansFromDegrees:fromCoord.longitude];
    
    double toLatRadians = asin(sin(fromLatRadians) * cos(distanceRadians)
                               + cos(fromLatRadians) * sin(distanceRadians) * cos(bearingRadians) );
    
    double toLonRadians = fromLonRadians + atan2(sin(bearingRadians)
                                                 * sin(distanceRadians) * cos(fromLatRadians), cos(distanceRadians)
                                                 - sin(fromLatRadians) * sin(toLatRadians));
    
    // adjust toLonRadians to be in the range -180 to +180...
    toLonRadians = fmod((toLonRadians + 3*M_PI), (2*M_PI)) - M_PI;
    
    CLLocationCoordinate2D result;
    result.latitude = [self degreesFromRadians:toLatRadians];
    result.longitude = [self degreesFromRadians:toLonRadians];
    
    return result;
}

+ (double)radiansFromDegrees:(double)degrees
{
    return degrees * (M_PI/180.0);
}

+ (double)degreesFromRadians:(double)radians
{
    return radians * (180.0/M_PI);
}

+ (CLLocationDistance) getDistanceFromLookingAtCoordinateForAltitude:(CLLocationDistance)eyeAltitude andPitch:(CGFloat)pitch
{
    CLLocationDistance distanceFromLookingAtCoord = eyeAltitude * tan(pitch * M_PI / 180.0);
    
    return distanceFromLookingAtCoord;
}

+ (NSString *)getTimeWithFormat:(NSDate *)keyframeTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"mm:ss"];
    NSString *formattedString = [dateFormatter stringFromDate:keyframeTime];
    
    return formattedString;
}

+ (NSString *)getAlphaCharacter:(int)intValue
{
    // ASCII to NSString
    NSString *ascString = [NSString stringWithFormat:@"%c", intValue+65]; // A for 0 based index
    
    return ascString;
    
}


+ (NSDictionary *)getEstimatedTimeForWaypointIndex:(NSInteger)indexPathRow
{
    FSDataManager *fsDataManager = [FSDataManager sharedManager];
    FSPathModelObject *currentPathModelObject = fsDataManager.currentPathModel;
    FSWaypointModelObject *waypointObject = [currentPathModelObject.waypointArray objectAtIndex:indexPathRow];

    if (indexPathRow == 0) {
        
        CGFloat altitude = waypointObject.waypoint.altitude;
        float estimatedTime = (altitude / DEFAULT_DRONE_VERTICAL_VELOCITY);

        waypointObject.timeFromPreviousWaypoint = estimatedTime;
        waypointObject.distanceFromHomeWaypoint = altitude;
    } else {
        
        FSWaypointModelObject *prevWaypointObject = [currentPathModelObject.waypointArray objectAtIndex:indexPathRow-1];
        
        CLLocation *newLocation = [[CLLocation alloc] initWithLatitude:waypointObject.waypoint.coordinate.latitude longitude:waypointObject.waypoint.coordinate.longitude];
        CLLocation *prevLocation = [[CLLocation alloc] initWithLatitude:prevWaypointObject.waypoint.coordinate.latitude longitude:prevWaypointObject.waypoint.coordinate.longitude];
        
        NSLog(@"Prev Location %@ New Location %@", prevLocation, newLocation);

        
        CLLocationDistance waypointDistance = [newLocation distanceFromLocation:prevLocation];
        CLLocationDistance altDistance = fabs(waypointObject.waypoint.altitude - prevWaypointObject.waypoint.altitude);
        
        CLLocationDistance distanceBetweenTwoCoordinates = sqrt(pow(waypointDistance,2) + pow(altDistance,2));
        waypointObject.distanceFromHomeWaypoint = distanceBetweenTwoCoordinates + prevWaypointObject.distanceFromHomeWaypoint;

        float estimatedTime = (distanceBetweenTwoCoordinates / 2.0);
        estimatedTime += prevWaypointObject.timeFromPreviousWaypoint + DEFAULT_DRONE_STAYTIME;
        waypointObject.timeFromPreviousWaypoint = estimatedTime;
    }
    
    
    float estimatedBatteryUsage = (waypointObject.timeFromPreviousWaypoint / TOTAL_PHANTOM3PRO_FLIGHT_TIME) * 100.0;
    
    estimatedBatteryUsage = (estimatedBatteryUsage > 100.0) ? 100.0 : estimatedBatteryUsage;
    
    NSDictionary *returnResultDict = @{ @"estimatedTime": [NSNumber numberWithFloat:waypointObject.timeFromPreviousWaypoint] ,
                                        @"estimatedBatteryUsage" : [NSNumber numberWithFloat:estimatedBatteryUsage] ,
                                        @"totalDistance" : [NSNumber numberWithFloat:waypointObject.distanceFromHomeWaypoint] ,
                                        };
    
    NSLog(@"%@", returnResultDict);

    return returnResultDict;
}

+ (double)toFeet:(CLLocationDistance)distanceInMeters
{
    return distanceInMeters*3.28084;
}

+ (CGFloat)convertToDJIAngle:(CGFloat)cameraPitch
{
    CGFloat djiAngle = cameraPitch - 90;
    return djiAngle;
}

+ (CGFloat)convertToCameraAngle:(CGFloat)djiPitch
{
    CGFloat cameraAngle = djiPitch + 90;
    return cameraAngle;
}


+ (CGFloat)convertToDJIHeading:(CGFloat)heading
{
    if (heading <= 180.0) {
        return heading;
    }
    
    return (heading-360.0);
}

+ (NSString *)convertTimeToString:(float) estimatedTime
{
    
    NSInteger elapsedSeconds = (NSInteger) estimatedTime;
    NSUInteger m = (elapsedSeconds / 60) % 60;
    NSUInteger s = elapsedSeconds % 60;
    
    NSString *formattedTime = [NSString stringWithFormat:@"%02lu:%02lu", m, s];

    return formattedTime;
}

+ (BOOL)isNumeric:(NSString *)string
{
    NSScanner *scanner = [NSScanner scannerWithString:string];
    BOOL isNumeric = [scanner scanDouble:NULL] && [scanner isAtEnd];

    return isNumeric;
}

@end
