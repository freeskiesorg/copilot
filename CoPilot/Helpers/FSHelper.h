//
//  FSHelper.h
//  CoPilot
//
//  Created by Pronob Ashwin on 6/29/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "FSWaypointModelObject.h"

@interface FSHelper : NSObject

+ (CLLocationCoordinate2D)coordinateFromCoord:(CLLocationCoordinate2D)fromCoord
                                 atDistanceKm:(double)distanceKm
                             atBearingDegrees:(double)bearingDegrees;

+ (CLLocationDistance) getDistanceFromLookingAtCoordinateForAltitude:(CLLocationDistance)eyeAltitude andPitch:(CGFloat)pitch;

+ (CLLocationCoordinate2D) getDroneCoordinateForMapCamera:(MKMapCamera *)mapCamera lookingAtCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate;

+ (CLLocationCoordinate2D) getDroneCoordinateForAltitude:(CLLocationDistance)altitude pitch:(CGFloat)pitch heading:(CLLocationDirection)heading lookingAtCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate;


+ (CLLocationCoordinate2D) getCameraCoordinateForCameraAltitude:(CLLocationDistance)cameraAltitude lookingAtCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate forMapCamera:(MKMapCamera *)mapCamera;
+ (CLLocationDistance) getDroneAltitudeForMapCameraAltitude:(CLLocationDistance)mapCameraAltitude;
+ (CLLocationCoordinate2D) getCameraCoordinateForCameraAltitude:(CLLocationDistance)cameraAltitude pitch:(CGFloat)pitch heading:(CLLocationDirection)heading lookingAtCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate ;

+ (CLLocationCoordinate2D) getMapCameraCoordinateForDroneAltitude:(CLLocationDistance)droneAltitude pitch:(CGFloat)pitch withBearing:(CLLocationDirection)bearing andDroneCoordinate:(CLLocationCoordinate2D)droneCoordinate;
+ (NSString *)getTimeWithFormat:(NSDate *)keyframeTime;
+ (NSString *)getAlphaCharacter:(int)intValue;
+ (NSDictionary *)getEstimatedTimeForWaypointIndex:(NSInteger)indexPathRow;
+ (double)toFeet:(CLLocationDistance)distanceInMeters;
+ (CGFloat)convertToDJIAngle:(CGFloat)cameraPitch;
+ (CGFloat)convertToCameraAngle:(CGFloat)djiPitch;

+ (double)radiansFromDegrees:(double)degrees;
+ (double)degreesFromRadians:(double)radians;

+ (CGFloat)convertToDJIHeading:(CGFloat)heading;

+ (NSString *)convertTimeToString:(float) estimatedTime;

+ (BOOL)isNumeric:(NSString *)string;

@end
