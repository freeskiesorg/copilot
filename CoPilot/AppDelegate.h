//
//  AppDelegate.h
//  CoPilot
//
//  Created by Pronob Ashwin on 6/2/15.
//  Copyright (c) 2015 FreeSkies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DJISDK/DJIAppManager.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, DJIAppManagerDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

