//
//  FSPathCollectionViewCell.m
//  CoPilot
//
//  Created by Pronob Ashwin on 7/14/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSPathCollectionViewCell.h"
#import "FSPathModelObject.h"

@interface FSPathCollectionViewCell ()


@property (weak, nonatomic) IBOutlet UILabel *pathLabel;

@end

@implementation FSPathCollectionViewCell


- (void)setPathLabelTextForPathModel:(FSPathModelObject *)pathModelObject
{
    self.pathLabel.text = pathModelObject.pathName;
}

- (IBAction)deleteButtonPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteCurrentCell:)]) {
        [self.delegate deleteCurrentCell:self];
    }
}

@end
