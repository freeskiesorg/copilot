//
//  FSPathCollectionViewCell.h
//  CoPilot
//
//  Created by Pronob Ashwin on 7/14/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSPathModelObject.h"

@class FSPathCollectionViewCell;

@protocol FSPathCollectionViewCellDelegate <NSObject>

-(void)deleteCurrentCell:(FSPathCollectionViewCell *)fsPathCollectionViewCell;

@end

@interface FSPathCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) id<FSPathCollectionViewCellDelegate> delegate;


- (void)setPathLabelTextForPathModel:(FSPathModelObject *)pathModelObject;

@end
