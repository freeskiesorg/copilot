//
//  FSWaypointCollectionViewCell.m
//  copilot
//
//  Created by Pronob Ashwin on 6/28/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSWaypointCollectionViewCell.h"
#import "FSHelper.h"

@interface FSWaypointCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *keyframeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;


@end

@implementation FSWaypointCollectionViewCell


- (void)animateActivityIndicatorOnCell:(BOOL)animating
{
    if (animating) {
        [self.activityIndicator startAnimating];
        self.userInteractionEnabled = NO;
        return;
    }
    
    [self.activityIndicator stopAnimating];
    self.userInteractionEnabled = YES;
}

- (void)setCellValuesForWaypointModel:(FSWaypointModelObject *)waypointModelObject alphaString:(NSString *)alphaString andEstimatedTime:(NSString *)estimatedTime
{
    self.keyframeLabel.text = [NSString stringWithFormat:@"KEYFRAME %@", alphaString];
    self.timeLabel.text = [NSString stringWithFormat:@"%@", estimatedTime];

    UIImage *mapImage = [UIImage imageWithData:waypointModelObject.mapImageData];
    
    self.mapImageView.image = mapImage;

}

@end
