//
//  FSWaypointCollectionViewCell.h
//  copilot
//
//  Created by Pronob Ashwin on 6/28/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSWaypointModelObject.h"

@interface FSWaypointCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mapImageView;

- (void)animateActivityIndicatorOnCell:(BOOL)animating;
- (void)setCellValuesForWaypointModel:(FSWaypointModelObject *)waypointModelObject alphaString:(NSString *)alphaString andEstimatedTime:(NSString *)estimatedTime
;

@end
