//
//  FSPathsViewController.m
//  CoPilot
//
//  Created by Pronob Ashwin on 7/10/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSPathsViewController.h"
#import "FSDataManager.h"
#import "FSPathModelObject.h"
#import "FSPathCollectionViewCell.h"
#import "FSGlobalDefines.h"
#import "FSDroneViewController.h"
#import "Flurry.h"

@interface FSPathsViewController () <UICollectionViewDataSource, UICollectionViewDelegate, FSPathCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (weak, nonatomic) IBOutlet UIButton *createNewButton;
@property (weak, nonatomic) IBOutlet UICollectionView *pathCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *standardMapImageView;
@property (weak, nonatomic) IBOutlet UIImageView *hybridMapImageView;
@property (strong, nonatomic) NSIndexPath *currentlySelectedIndex;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@end

static NSString * const pathIdentifier = @"pathCell";


@implementation FSPathsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.createNewButton.layer.borderWidth = 2;
    self.createNewButton.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.currentlySelectedIndex = nil;
    self.pathCollectionView.alwaysBounceVertical = YES;
    [self blurBackgroundImage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.pathCollectionView reloadData];
    [self getVersionStrings];
}


- (void)getVersionStrings
{
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    NSString * versionBuildString = [NSString stringWithFormat:@"VERSION: %@ b%@", appVersionString, appBuildString];
    [Flurry logEvent:versionBuildString];
    self.versionLabel.text = versionBuildString;

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    FSDataManager *fsDataManager = [FSDataManager sharedManager];
    
    if ([segue.identifier isEqualToString:@"createPathSegue"]) {
        if (self.currentlySelectedIndex != nil) {
            fsDataManager.currentPathModel = [fsDataManager.pathsArray objectAtIndex:self.currentlySelectedIndex.row];
        } else {
            [fsDataManager createPathObject];
        }
        return;
    }
}



#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[FSDataManager sharedManager].pathsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FSDataManager *fsDataManager = [FSDataManager sharedManager];
    
    FSPathCollectionViewCell *cell = (FSPathCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:pathIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    FSPathModelObject *pathModel = [fsDataManager.pathsArray objectAtIndex:indexPath.row];
    
    [cell setPathLabelTextForPathModel:pathModel];
    
    if (cell.isSelected) {
        [cell setBackgroundColor:[UIColor darkGrayColor]];
    } else {
        [cell setBackgroundColor:[UIColor blackColor]];
    }
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
 }
 
 - (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
 }
 
 - (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
 }
 */


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FSDataManager *fsDataManager = [FSDataManager sharedManager];
    
    FSPathCollectionViewCell *cell = (FSPathCollectionViewCell *)[self.pathCollectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor darkGrayColor];
    
    FSPathModelObject *pathModelObject = [fsDataManager.pathsArray objectAtIndex:indexPath.row];
    
    UIImage *stdMapImage = [UIImage imageWithData:pathModelObject.standardMapImageData];
    self.standardMapImageView.image = stdMapImage;

    UIImage *hybMapImage = [UIImage imageWithData:pathModelObject.hybridMapImageData];
    self.hybridMapImageView.image = hybMapImage;
    
    self.currentlySelectedIndex = indexPath;
    
    // Capture author info & user status
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   pathModelObject.pathName, @"PathName",
                                   [NSNumber numberWithUnsignedInteger:pathModelObject.waypointArray.count], @"WaypointCount",nil];
    
    [Flurry logEvent:@"User Clicked on Path" withParameters:articleParams];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FSPathCollectionViewCell *cell = (FSPathCollectionViewCell *)[self.pathCollectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor blackColor];
}

// ImageEFX
#pragma mark image EFX

- (void)blurBackgroundImage {
    // create effect
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    // add effect to an effect view
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    effectView.frame = self.view.frame;
    
    effectView.alpha = 0.7;
    [self.backgroundImageView addSubview:effectView];
}

- (IBAction)createNewButtonPressed:(id)sender {
    if(self.currentlySelectedIndex != nil) {
        [self.pathCollectionView deselectItemAtIndexPath:self.currentlySelectedIndex animated:NO];
        self.currentlySelectedIndex = nil;
    }
    [self performSegueWithIdentifier:@"createPathSegue" sender:self];
}

- (IBAction)goButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"createPathSegue" sender:self];
}

#pragma mark FSPathCollectionViewCell

-(void)deleteCurrentCell:(FSPathCollectionViewCell *)fsPathCollectionViewCell
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Delete" message:@"Are you sure you want to delete this mission?" preferredStyle:UIAlertControllerStyleAlert];
    
    
    WeakSelf weakSelf = self;
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * __nonnull action) {
        if (weakSelf) {
            StrongSelf strongSelf = weakSelf;
            
            [strongSelf.pathCollectionView deselectItemAtIndexPath:self.currentlySelectedIndex animated:NO];
            
            NSIndexPath *currentIndexPath = [strongSelf.pathCollectionView indexPathForCell:fsPathCollectionViewCell];
            
            FSPathModelObject *pathsObject = [[FSDataManager sharedManager].pathsArray objectAtIndex:currentIndexPath.row];

            [[FSDataManager sharedManager] removePath:pathsObject];
            strongSelf.currentlySelectedIndex = nil;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                strongSelf.standardMapImageView.image = [UIImage imageNamed:@"StdMap"];
                strongSelf.hybridMapImageView.image = [UIImage imageNamed:@"HybridMap"];
                [strongSelf.pathCollectionView reloadData];
            });
        }
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];

}

@end
