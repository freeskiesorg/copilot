//
//  FSDroneMapController.m
//  copilot
//
//  Created by Pronob Ashwin on 7/26/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSDroneMapController.h"

@implementation FSDroneMapController


- (instancetype)init
{
    if (self = [super init]) {
        self.wayPoints = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)addWaypoint:(CLLocationCoordinate2D)coordinate forMapView:(MKMapView *)mapView
{
    CLLocation *location = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    [_wayPoints addObject:location];
    MKPointAnnotation* annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = location.coordinate;
    [mapView addAnnotation:annotation];
}

- (void)deleteAllWayPointsForMapView:(MKMapView *)mapView
{
    [_wayPoints removeAllObjects];
    NSArray* annos = [NSArray arrayWithArray:mapView.annotations];
    for (int i = 0; i < annos.count; i++) {
        id<MKAnnotation> ann = [annos objectAtIndex:i];
        [mapView removeAnnotation:ann];
    }
}

- (NSArray *)wayPoints
{
    return self.wayPoints;
}

@end
