//
//  FSDroneMapController.h
//  copilot
//
//  Created by Pronob Ashwin on 7/26/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>

@interface FSDroneMapController : NSObject

@property (strong, nonatomic) NSMutableArray *wayPoints;

/**
 * Add Waypoints in the Map View
 */
- (void)addWaypoint:(CLLocationCoordinate2D)coordinate forMapView:(MKMapView *)mapView;

/**
 *  Delete All Waypoints in Map View
 */
- (void)deleteAllWayPointsForMapView:(MKMapView *)mapView;

/**
 *  Current Edit Points
 *
 *  @return Return an NSArray contains multiple CCLocation objects
 */
- (NSArray *)wayPoints;


@end
