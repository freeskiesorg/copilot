//
//  PageContentViewController.m
//  copilot
//
//  Created by Pronob Ashwin on 7/22/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "PageContentViewController.h"

@interface PageContentViewController () <UIGestureRecognizerDelegate>

@end

@implementation PageContentViewController


-(void)viewDidLoad
{
    [super viewDidLoad];
    self.imageView.image = [UIImage imageNamed:self.imageFile];
    self.nextButton.titleLabel.text = self.txtTitle;
    
    
    UITapGestureRecognizer *tapGuesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGuesture.delegate = self;
    [self.view addGestureRecognizer:tapGuesture];

}

////////////////////////////////////////////////////////////
// UIGestureRecognizerDelegate methods

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.nextButton]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }

    if ([touch.view isDescendantOfView:self.imageView]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }

    return YES;
}


- (void)handleTapGesture:(UIGestureRecognizer *)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(dismissPageViewController:)]) {
        [self.delegate dismissPageViewController:self];
    }
}


@end
