//
//  SourceForPageViewController.m
//  CoPilot
//
//  Created by Pronob Ashwin on 7/22/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "SourceForPageViewController.h"
#import "FSGlobalDefines.h"

@interface SourceForPageViewController () <UIPageViewControllerDelegate, PageContentViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *nextButton;


@end

@implementation SourceForPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _pageImages = @[@"FMode", @"PMode", @"PreFlight"];
    _pageTitles = @[@"NEXT", @"NEXT", @"LAUNCH MISSION"];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    startingViewController.delegate = self;
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 200);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];

    for (UIScrollView *view in self.pageViewController.view.subviews) {
        
        if ([view isKindOfClass:[UIScrollView class]]) {
            
            view.scrollEnabled = NO;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}


- (IBAction)nextButtonPressed:(id)sender {
    NSUInteger indexToShow = self.currentIndex;
    
    if (self.currentIndex != [self.pageTitles count]) {
        self.currentIndex = ++indexToShow;
    }

    if (indexToShow == [self.pageTitles count]) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(sourceForPageViewController:didPressButtonIndex:withCompletion:)]) {
            [self.delegate sourceForPageViewController:self didPressButtonIndex:indexToShow withCompletion:nil];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }


    WeakSelf weakSelf = self;
    if (self.delegate && [self.delegate respondsToSelector:@selector(sourceForPageViewController:didPressButtonIndex:withCompletion:)]) {
        [self.delegate sourceForPageViewController:self didPressButtonIndex:indexToShow withCompletion:^(BOOL completion) {
            if (weakSelf) {
                if (completion) {
                    [weakSelf showPageForIndex:indexToShow];
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf dismissViewControllerAnimated:YES completion:nil];
                    });
                }
            }
        }];
    }
}

- (void)showPageForIndex:(NSUInteger)indexToShow
{
    PageContentViewController *startingViewController = [self viewControllerAtIndex:indexToShow];
    startingViewController.delegate = self;

    NSArray *viewControllers = @[startingViewController];
    [self.nextButton setTitle:self.pageTitles[indexToShow] forState:UIControlStateNormal]; ;
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.txtTitle = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return self.currentIndex;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    //get current index of current page
    PageContentViewController *theCurrentViewController = [self.pageViewController.viewControllers objectAtIndex:0];
    self.currentIndex = theCurrentViewController.pageIndex;
    [self.nextButton setTitle:self.pageTitles[self.currentIndex] forState:UIControlStateNormal];
}

#pragma mark PageContentViewControllerDelegate

- (void)dismissPageViewController:(PageContentViewController *)pageContentViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
