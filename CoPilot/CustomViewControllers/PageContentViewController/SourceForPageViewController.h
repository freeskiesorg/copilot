//
//  SourceForPageViewController.h
//  CoPilot
//
//  Created by Pronob Ashwin on 7/22/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

typedef void (^PageDelegateBlock)(BOOL completion);
typedef void (^UploadTaskBlock)(BOOL uploadTaskDone);


@class SourceForPageViewController;

@protocol SourceForPageViewControllerDelegate <NSObject>

- (void)sourceForPageViewController:(SourceForPageViewController *)pageViewController didPressButtonIndex:(NSInteger)buttonIndex withCompletion:(PageDelegateBlock)completion;

@end

@interface SourceForPageViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@property (weak, nonatomic) id<SourceForPageViewControllerDelegate> delegate;

@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;

@property NSUInteger currentIndex;

@end
