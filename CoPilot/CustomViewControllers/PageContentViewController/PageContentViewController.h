//
//  PageContentViewController.h
//  copilot
//
//  Created by Pronob Ashwin on 7/22/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PageContentViewController;

@protocol PageContentViewControllerDelegate <NSObject>

- (void)dismissPageViewController:(PageContentViewController *)pageContentViewController;

@end

@interface PageContentViewController : UIViewController

@property  NSUInteger pageIndex;
@property  NSString *imageFile;
@property  NSString *txtTitle;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property (weak, nonatomic) id<PageContentViewControllerDelegate> delegate;

@end
