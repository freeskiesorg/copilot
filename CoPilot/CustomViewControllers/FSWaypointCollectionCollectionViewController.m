//
//  UIWaypointCollectionCollectionViewController.m
//  CoPilot
//
//  Created by Pronob Ashwin on 6/12/15.
//  Copyright (c) 2015 FreeSkies. All rights reserved.
//

#import "FSWaypointCollectionCollectionViewController.h"
#import "FSDataManager.h"
#import "FSWaypointCollectionViewCell.h"
#import "FSGlobalDefines.h"
#import "FSAddCollectionViewCell.h"
#import "FSHelper.h"


static NSString * const waypointIdentifier = @"waypointCell";
static NSString * const addCellIdentifier = @"addCell";


@interface FSWaypointCollectionCollectionViewController () <UIGestureRecognizerDelegate>

@property (nonatomic) BOOL initialScroll;
@end

@implementation FSWaypointCollectionCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.alwaysBounceHorizontal = YES;
    self.currentIndexPath = nil;
    self.initialScroll = NO;
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.delegate = self;
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    
    [self.collectionView addGestureRecognizer:tapGesture];

    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)resetCollectionView {
    [self.collectionView deselectItemAtIndexPath:self.currentIndexPath animated:YES];
    [self.collectionView reloadData];
}

#pragma mark tap gesture

- (void)handleTapGesture:(UIGestureRecognizer *)sender
{
    [self resetCollectionView];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(clearSelectionForWaypointCollectionViewController:)]) {
        [self.delegate clearSelectionForWaypointCollectionViewController:self];
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[UICollectionView class]]) //It can work for any class you do not want to receive touch
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[FSDataManager sharedManager].currentPathModel.waypointArray count] + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FSDataManager *fsDataManager = [FSDataManager sharedManager];
    
    if (indexPath.row == [fsDataManager.currentPathModel.waypointArray count]) {
        FSAddCollectionViewCell *cell = (FSAddCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:addCellIdentifier forIndexPath:indexPath];
        cell.borderView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        cell.borderView.layer.borderWidth = 2.0;

        return cell;
    }
    
    FSWaypointCollectionViewCell *cell = (FSWaypointCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:waypointIdentifier forIndexPath:indexPath];
    FSWaypointModelObject *waypointModel = [fsDataManager.currentPathModel.waypointArray objectAtIndex:indexPath.row];
    
    NSDictionary *dictionary = [FSHelper getEstimatedTimeForWaypointIndex:indexPath.row];
    NSNumber *estimatedTime = [dictionary objectForKey:@"estimatedTime"];
    
    NSString *estimatedTimeString = [FSHelper convertTimeToString:[estimatedTime floatValue]];
    
    [cell setCellValuesForWaypointModel:waypointModel alphaString:waypointModel.alphaLabel andEstimatedTime:estimatedTimeString];
    
    if (cell.isSelected) {
        cell.mapImageView.layer.borderColor = [UIColor cyanColor].CGColor;
        cell.mapImageView.layer.borderWidth = 2.0;
    } else {
        cell.mapImageView.layer.borderColor = [UIColor clearColor].CGColor;
        cell.mapImageView.layer.borderWidth = 1.0;
    }
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

- (void)refreshCurrentCell
{
    [self.collectionView performBatchUpdates:^{
        [self.collectionView reloadItemsAtIndexPaths:@[self.currentIndexPath]];
    } completion:nil];
    
}

- (NSString *)createWaypointName
{
    NSString *waypointName = [NSString stringWithFormat:@"waypoint%lu", (unsigned long)[[FSDataManager sharedManager].currentPathModel.waypointArray count]];
    return waypointName;
}

- (void)animateCurrentlySelectedCell:(BOOL)animate
{
    if (self.currentIndexPath != nil) {
        FSWaypointCollectionViewCell *cell = (FSWaypointCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:self.currentIndexPath];
        [cell animateActivityIndicatorOnCell:animate];
    }
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    WeakSelf weakSelf = self;

    FSDataManager *fsDataManager = [FSDataManager sharedManager];
    // Add pressed
    if (indexPath.row == [fsDataManager.currentPathModel.waypointArray count]) {
        NSLog(@"Add %ld", (long)indexPath.row);

        self.currentIndexPath = nil;
        if (self.delegate && [self.delegate respondsToSelector:@selector(addNewWaypointToCollectionViewController:withCompletion:)]) {
            
            __block FSAddCollectionViewCell *cell = (FSAddCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            [cell animateActivityIndicatorOnCell:YES];
            
            [self.delegate addNewWaypointToCollectionViewController:self withCompletion:^(BOOL finished) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    StrongSelf strongSelf = weakSelf;
                    if (strongSelf) {
                        [cell animateActivityIndicatorOnCell:NO];
                        [strongSelf.collectionView reloadData];
                        strongSelf.initialScroll = NO;
                    }
                });
            }];
        }
        return;
    }
    
    FSWaypointCollectionViewCell *cell = (FSWaypointCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    cell.mapImageView.layer.borderColor = [UIColor colorWithRed:0.29 green:0.56 blue:0.88 alpha:1.0].CGColor;
    cell.mapImageView.layer.borderWidth = 2.0;
    
    self.currentIndexPath = indexPath;
    FSWaypointModelObject *waypointModelObject = [fsDataManager.currentPathModel.waypointArray objectAtIndex:indexPath.row];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(waypointCollectionViewController:updateMapWithWaypointData:)]) {
        [self.delegate waypointCollectionViewController:self updateMapWithWaypointData:waypointModelObject];
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FSWaypointCollectionViewCell *cell = (FSWaypointCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    cell.mapImageView.layer.borderColor = [UIColor clearColor].CGColor;
    cell.mapImageView.layer.borderWidth = 1.0;
}


- (void)viewDidLayoutSubviews
{
    [self.collectionView layoutIfNeeded];

    if (!self.initialScroll) {
        self.initialScroll = YES;
        FSDataManager *fsDataManager = [FSDataManager sharedManager];
        
        NSArray *visibleItems = [self.collectionView indexPathsForVisibleItems];
        NSIndexPath *currentItem = [visibleItems lastObject];
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:[fsDataManager.currentPathModel.waypointArray count] inSection:currentItem.section];
        
        [self.collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    }
}

@end
