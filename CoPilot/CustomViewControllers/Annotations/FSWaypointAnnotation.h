//
//  FSWaypointAnnotation.h
//  copilot
//
//  Created by Pronob Ashwin on 7/26/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>

@interface FSWaypointAnnotation : MKPointAnnotation

@property (strong, nonatomic) NSString *waypointLabel;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate andWaypointLabel:(NSString *)waypointLabel;

- (void)updateWaypointLabel:(NSString *)waypointLabel;

@end
