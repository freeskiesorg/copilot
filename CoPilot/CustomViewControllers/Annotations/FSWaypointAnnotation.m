//
//  FSWaypointAnnotation.m
//  copilot
//
//  Created by Pronob Ashwin on 7/26/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSWaypointAnnotation.h"

@implementation FSWaypointAnnotation

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate andWaypointLabel:(NSString *)waypointLabel
{
    self = [super init];
    
    if (self) {
        self.coordinate = coordinate;
        _waypointLabel = waypointLabel;
    }
    
    return self;
}

- (void)updateWaypointLabel:(NSString *)waypointLabel
{
    self.waypointLabel = waypointLabel;
}

@end
