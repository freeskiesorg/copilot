//
//  FSOverlayPathRenderer.h
//  copilot
//
//  Created by Pronob Ashwin on 7/28/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface FSOverlayPathRenderer : MKOverlayPathRenderer

@end
