//
//  FSDroneAnnotation.h
//  copilot
//
//  Created by Pronob Ashwin on 7/26/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "FSDroneAnnotationView.h"

@interface FSDroneAnnotation : NSObject<MKAnnotation>

@property(nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property(nonatomic, weak) FSDroneAnnotationView* annotationView;

-(id) initWithCoordiante:(CLLocationCoordinate2D)coordinate;

-(void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;

-(void) updateHeading:(float)heading;

@end
