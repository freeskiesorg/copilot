//
//  FSOverlayPathRenderer.m
//  copilot
//
//  Created by Pronob Ashwin on 7/28/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSOverlayPathRenderer.h"

@implementation FSOverlayPathRenderer

- (void)createPath
{
    MKPolyline *line = (id)self.overlay;
    
    MKMapPoint *points = line.points;
    NSUInteger pointCount = line.pointCount;
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, points[0].x, points[0].y);
    
    for (int i = 1; i < pointCount; i++) {
        CGPathAddLineToPoint(path, NULL, points[i].x, points[i].y);
    }
    [self setPath:path];
}

@end
