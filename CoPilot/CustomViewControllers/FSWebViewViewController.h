//
//  FSWebViewViewController.h
//  CoPilot
//
//  Created by Pronob Ashwin on 7/9/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FSWebViewViewController : UIViewController

@property (nonatomic, strong) NSString *webURLString;

@end
