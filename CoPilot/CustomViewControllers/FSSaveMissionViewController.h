//
//  FSSaveMissionViewController.h
//  CoPilot
//
//  Created by Pronob Ashwin on 7/17/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FSSaveMissionViewController : UIViewController

- (void)refreshStandardImage:(NSData *)imageData;
- (void)refreshHybridImage:(NSData *)imageData;


@end
