//
//  WaypointAnnotationView.m
//  copilot
//
//  Created by Pronob Ashwin on 7/25/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "WaypointAnnotationView.h"

@implementation WaypointAnnotationView

- (id) initWithAnnotation:(nullable id<MKAnnotation>)annotation reuseIdentifier:(nullable NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    // Annotation Image
    
    UIImage *waypointImage = [UIImage imageNamed:@"WaypointImage"];
    self.image = waypointImage;
    self.backgroundColor = [UIColor clearColor];
    
    self.canShowCallout = YES;
    
    return self;
}

@end
