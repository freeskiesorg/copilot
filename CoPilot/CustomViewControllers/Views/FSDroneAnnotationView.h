//
//  FSDroneAnnotationView.h
//  copilot
//
//  Created by Pronob Ashwin on 7/26/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface FSDroneAnnotationView : MKAnnotationView

-(void) updateHeading:(float)heading;


@end
