//
//  FSDroneAnnotationView.m
//  copilot
//
//  Created by Pronob Ashwin on 7/26/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSDroneAnnotationView.h"

@implementation FSDroneAnnotationView

- (instancetype)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.enabled = NO;
        self.draggable = NO;
        self.image = [UIImage imageNamed:@"Location Arrow"];
    }
    
    return self;
}

-(void) updateHeading:(float)heading
{
    self.transform = CGAffineTransformIdentity;
    self.transform = CGAffineTransformMakeRotation(heading);
}

@end