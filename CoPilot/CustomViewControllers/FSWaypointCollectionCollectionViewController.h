//
//  UIWaypointCollectionCollectionViewController.h
//  CoPilot
//
//  Created by Pronob Ashwin on 6/12/15.
//  Copyright (c) 2015 FreeSkies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSWaypointModelObject.h"

typedef void (^DelegateCompletionBlock)(BOOL finished);


@protocol FSWaypointCollectionCollectionViewControllerDelegate;

@interface FSWaypointCollectionCollectionViewController : UICollectionViewController

@property (weak, nonatomic) id<FSWaypointCollectionCollectionViewControllerDelegate> delegate;
@property (strong, nonatomic) NSIndexPath *currentIndexPath;

- (void)refreshCurrentCell;
- (NSString *)createWaypointName;
- (void)animateCurrentlySelectedCell:(BOOL)animate;
- (void)resetCollectionView;
@end


@protocol FSWaypointCollectionCollectionViewControllerDelegate <NSObject>

- (void)addNewWaypointToCollectionViewController:(id)waypointCollectionViewController withCompletion:(DelegateCompletionBlock)completion;
- (void)waypointCollectionViewController:(id)waypointCollectionViewController updateMapWithWaypointData:(FSWaypointModelObject *)waypointModelObject;
- (void)clearSelectionForWaypointCollectionViewController:(id)waypointCollectionViewController;


@end