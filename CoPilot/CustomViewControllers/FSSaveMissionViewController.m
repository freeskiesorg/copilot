//
//  FSSaveMissionViewController.m
//  CoPilot
//
//  Created by Pronob Ashwin on 7/17/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSSaveMissionViewController.h"
#import "FSPathModelObject.h"
#import "FSDataManager.h"
#import "FSGlobalDefines.h"

@interface FSSaveMissionViewController () <UIGestureRecognizerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *saveTextField;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UIImageView *map2DImageView;
@property (weak, nonatomic) IBOutlet UIImageView *map3DImageView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *standardActivityIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *hybridActivityIndicator;

@end

@implementation FSSaveMissionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIColor *color = [UIColor darkGrayColor];
    self.saveTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"CLICK TO TYPE MISSION NAME" attributes:@{NSForegroundColorAttributeName: color}];
    self.saveTextField.delegate = self;
    
    UITapGestureRecognizer *tapGuesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGuesture.delegate = self;
    [self.view addGestureRecognizer:tapGuesture];

    if (self.map2DImageView.image == nil) {
        [self.standardActivityIndicator startAnimating];
    }
    if (self.map3DImageView.image == nil) {
        [self.hybridActivityIndicator startAnimating];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    self.saveTextField.text = [FSDataManager sharedManager].currentPathModel.pathName;
}

////////////////////////////////////////////////////////////
// UIGestureRecognizerDelegate methods

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.nextButton]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)refreshStandardImage:(NSData *)imageData
{
    self.map2DImageView.image = [UIImage imageWithData:imageData];
    [self.standardActivityIndicator stopAnimating];
}

- (void)refreshHybridImage:(NSData *)imageData
{
    self.map3DImageView.image = [UIImage imageWithData:imageData];
    [self.hybridActivityIndicator stopAnimating];
}


- (void)checkForSave
{
    if (self.saveTextField.text.length > 0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Save Mission" message:@"Do you want to save your mission?" preferredStyle:UIAlertControllerStyleAlert];
        
        
        WeakSelf weakSelf = self;
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Discard" style:UIAlertActionStyleCancel handler:^(UIAlertAction * __nonnull action) {
            if (weakSelf) {
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }
        }];

        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * __nonnull action) {
            if (weakSelf){
                [[FSDataManager sharedManager] saveCurrentPathWithPathName:weakSelf.saveTextField.text];
            }
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)saveMission
{
    if (self.saveTextField.text.length == 0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Save Mission" message:@"You need to enter a name to save your Mission." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    
    FSDataManager *fsDataManager = [FSDataManager sharedManager];
    
    [fsDataManager saveCurrentPathWithPathName:self.saveTextField.text];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)backButtonPressed:(id)sender {
    [self checkForSave];
}

- (IBAction)nextButtonPressed:(id)sender {
    [self saveMission];
}


- (void)handleTapGesture:(UIGestureRecognizer *)sender
{
    [self checkForSave];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self saveMission];
    return YES;
}

@end
