//
//  FSSearchViewController.h
//  copilot
//
//  Created by Pronob Ashwin on 7/20/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#define MY_CURRENT_LOCATION @"MY CURRENT LOCATION"

@protocol FSSearchViewControllerDelegate <NSObject>

- (void)searchForNewLocation:(NSString *)locationString withPlaceMark:(CLPlacemark *)placemark;

@end

@interface FSSearchViewController : UIViewController <CLLocationManagerDelegate>

@property (weak, nonatomic) id<FSSearchViewControllerDelegate> delegate;
@property (nonatomic, strong) NSArray *places;

@end
