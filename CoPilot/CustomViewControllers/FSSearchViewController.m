//
//  FSSearchViewController.m
//  copilot
//
//  Created by Pronob Ashwin on 7/20/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSSearchViewController.h"
#import "FSGlobalDefines.h"
#import <MapKit/MapKit.h>


#pragma mark -

static NSString *kCellIdentifier = @"searchCell";

@interface FSSearchViewController () <UIGestureRecognizerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) MKLocalSearch *localSearch;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic) CLLocationCoordinate2D userLocation;
@property (nonatomic, assign) MKCoordinateRegion boundingRegion;

@property (weak, nonatomic) IBOutlet UITextField *currentLocationTextField;
@property (weak, nonatomic) IBOutlet UITableView *searchTableView;


@end

@implementation FSSearchViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.currentLocationTextField.delegate = self;
    [self.searchTableView setBackgroundColor:[UIColor clearColor]];
    //Remove the empty cells
    self.searchTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    // start by locating user's current position
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];

    UITapGestureRecognizer *tapGuesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGuesture.delegate = self;
    [self.view addGestureRecognizer:tapGuesture];

}

- (IBAction)currentLocationPressed:(id)sender {
    self.currentLocationTextField.text = MY_CURRENT_LOCATION;
}

- (IBAction)nextButtonPressed:(id)sender {

    CLPlacemark *placeMark = (self.places.count > 0) ? [self.places firstObject] : nil;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchForNewLocation: withPlaceMark:)]) {
        [self.delegate searchForNewLocation:self.currentLocationTextField.text withPlaceMark:placeMark];
    }

    [self dismissViewControllerAnimated:YES completion:nil];
}

////////////////////////////////////////////////////////////
// UIGestureRecognizerDelegate methods

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.searchTableView]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}

- (void)handleTapGesture:(UIGestureRecognizer *)sender
{
    if ([self.currentLocationTextField isFirstResponder]) {
        [self.currentLocationTextField resignFirstResponder];
        return;
    }

    [self checkForSave];
}


#pragma mark - UITableView delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.places count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    CLPlacemark *placemark = [self.places objectAtIndex:indexPath.row];
    
    NSArray *lines = placemark.addressDictionary[@"FormattedAddressLines"];
    NSString *addressString = [lines componentsJoinedByString:@", "];
    
    cell.textLabel.text = addressString;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // pass the individual place to our map destination view controller
    NSIndexPath *selectedItem = [self.searchTableView indexPathForSelectedRow];
    
    CLPlacemark *placeMark = [self.places objectAtIndex:selectedItem.row];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchForNewLocation:withPlaceMark:)]) {
        [self.delegate searchForNewLocation:placeMark.name withPlaceMark:placeMark];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)startSearchWithGeocoding:(NSString *)searchString
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:searchString
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     if (placemarks && placemarks.count > 0) {
                         self.places = placemarks;
                         
                         CLPlacemark *topResult = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];

                         MKCoordinateRegion region;
                         region.center = placemark.region.center;
                         region.span.longitudeDelta /= 8.0;
                         region.span.latitudeDelta /= 8.0;

                         self.boundingRegion = region;
                         
                         [self.searchTableView reloadData];
                     }
                 }];
}

- (void)checkForSave
{
    if (self.currentLocationTextField.text.length > 0) {
        
        if ([self.currentLocationTextField.text isEqualToString:MY_CURRENT_LOCATION]) {
            [self dismissViewControllerAnimated:YES completion:nil];
            return;
        }
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Wait" message:@"Are you sure you want to cancel?" preferredStyle:UIAlertControllerStyleAlert];
        
        
        WeakSelf weakSelf = self;
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * __nonnull action) {
            if (weakSelf) {
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)startSearch:(NSString *)searchString
{
    if (self.localSearch.searching)
    {
        [self.localSearch cancel];
    }
    
    // confine the map search area to the user's current location
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = self.userLocation.latitude;
    newRegion.center.longitude = self.userLocation.longitude;
    
    // setup the area spanned by the map region:
    // we use the delta values to indicate the desired zoom level of the map,
    //      (smaller delta values corresponding to a higher zoom level)
    //
    newRegion.span.latitudeDelta = 0.112872;
    newRegion.span.longitudeDelta = 0.109863;
    
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    
    request.naturalLanguageQuery = searchString;
    request.region = newRegion;
    
    MKLocalSearchCompletionHandler completionHandler = ^(MKLocalSearchResponse *response, NSError *error)
    {
        if (error != nil)
        {
//            NSString *errorStr = [[error userInfo] valueForKey:NSLocalizedDescriptionKey];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Could not find places"
//                                                            message:errorStr
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
        }
        else
        {
            self.places = [response mapItems];
            
            // used for later when setting the map's region in "prepareForSegue"
            self.boundingRegion = response.boundingRegion;
            
            [self.searchTableView reloadData];
        }
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    };
    
    if (self.localSearch != nil)
    {
        self.localSearch = nil;
    }
    self.localSearch = [[MKLocalSearch alloc] initWithRequest:request];
    
    [self.localSearch startWithCompletionHandler:completionHandler];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (BOOL)isLocalSearchEnabled
{
    // check to see if Location Services is enabled, there are two state possibilities:
    // 1) disabled for entire device, 2) disabled just for this app
    //
    NSString *causeStr = nil;
    
    // check whether location services are enabled on the device
    if ([CLLocationManager locationServicesEnabled] == NO)
    {
        causeStr = @"device";
    }
    // check the application’s explicit authorization status:
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        causeStr = @"app";
    }
    else
    {
        // we are good to go, start the search
        return YES;
    }
    
    if (causeStr != nil)
    {
        NSString *alertMessage = [NSString stringWithFormat:@"You currently have location services disabled for this %@. Please refer to \"Settings\" app to turn on Location Services.", causeStr];
        
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled"
                                                                        message:alertMessage
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
        [servicesDisabledAlert show];
    }
    
    return NO;
}

- (BOOL)textField:(nonnull UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(nonnull NSString *)string
{
    if (![self isLocalSearchEnabled]) {
        return NO;
    }
    
    //because the view can be unloaded we must store all data so that its state can be restored in viewDidLoad
    NSString *searchQuery = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    SEL fetchSearchResults = @selector(startSearchWithGeocoding:);
    
    //cancel the previous search request
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:fetchSearchResults object:nil];
    
    //perform the search in a seconds time. If the user enters addition data then this search will be cancelled by the previous line
    [self performSelector:fetchSearchResults withObject:searchQuery afterDelay:1];
    
    return YES;
}





#pragma mark - CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // remember for later the user's current location
    self.userLocation = newLocation.coordinate;
    
    [manager stopUpdatingLocation]; // we only want one update
    
    manager.delegate = nil;         // we might be called again here, even though we
    // called "stopUpdatingLocation", remove us as the delegate to be sure
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    // report any errors returned back from Location Services
}


@end
