//
//  FSGlobalDefines.h
//  copilot
//
//  Created by Pronob Ashwin on 6/28/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#ifndef FSGlobalDefines_h
#define FSGlobalDefines_h

#define WeakSelf __weak typeof (self)
#define StrongSelf __strong typeof(weakSelf)

#define DRONE_CAMERA_COMPENSATION_FACTOR 0.66
#define DRONE_CAMERA_REVERSE_COMPENSATION_FACTOR 1.5
#define ALTITUDE_CAP 123.0 * DRONE_CAMERA_REVERSE_COMPENSATION_FACTOR

#define EARTHS_RADIUS_METERS 6371000

#define CLCOORDINATES_EQUAL( coord1, coord2 ) (coord1.latitude == coord2.latitude && coord1.longitude == coord2.longitude)

#define TOTAL_PHANTOM3PRO_FLIGHT_TIME 1080 // Seconds ie 18 mins

#define PLAYBACK_ANIMATION_SPEED 5.0
#define UPDATE_ANIMATION_SPEED 2.0

#define DEFAULT_DRONE_VERTICAL_VELOCITY 2.0
#define DEFAULT_DRONE_STAYTIME 6.0


#define METER_TO_MILES = 0.000621371192237
#define MILES_TO_KILOMETERS 1.609344
#define KILOMETERS_TO_MILES 1/MILES_TO_KILOMETERS
#define MILES_TO_METER 1/METER_TO_MILES
#define FEET_TO_MILES 0.000189394
#define FEET_TO_METERS 0.3048
#define METERS_TO_FEET 1/FEET_TO_METERS
#define MILES_TO_FEET 1/FEET_TO_MILES
#define FEET_PER_METER 3.28084
#define KILOMETERS_PER_DEGREE 111
#define MAP_MIN_METERS_PER_PIXEL 0.23544811562


#endif /* FSGlobalDefines_h */
