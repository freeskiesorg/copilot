//
//  FSEnableDroneIAPHelper.m
//  copilot
//
//  Created by Pronob Ashwin on 10/22/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSEnableDroneIAPHelper.h"

@implementation FSEnableDroneIAPHelper

+ (FSEnableDroneIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static FSEnableDroneIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"com.freeskies.iap.enabled",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

@end
