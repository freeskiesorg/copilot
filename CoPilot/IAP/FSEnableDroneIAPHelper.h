//
//  FSEnableDroneIAPHelper.h
//  copilot
//
//  Created by Pronob Ashwin on 10/22/15.
//  Copyright © 2015 FreeSkies. All rights reserved.
//

#import "FSIAPHelper.h"

@interface FSEnableDroneIAPHelper : FSIAPHelper

+ (FSEnableDroneIAPHelper *)sharedInstance;

@end
