//
//  CoPilotTests.m
//  CoPilotTests
//
//  Created by Pronob Ashwin on 6/2/15.
//  Copyright (c) 2015 FreeSkies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "FSHelper.h"

@interface CoPilotTests : XCTestCase

@end

@implementation CoPilotTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testGetDistanceFromLookingAtCoordinateForAltitude {
    CLLocationDistance distance = [FSHelper getDistanceFromLookingAtCoordinateForAltitude:10 andPitch:45.0];
    
    NSLog(@"distance %f", distance);
    
}


- (void)testGetDroneCoordinateForAltitude {
    CLLocationCoordinate2D eyeCoordinate = CLLocationCoordinate2DMake(37.421424, -122.197812);
    CLLocationCoordinate2D coordinate = [FSHelper getDroneCoordinateForAltitude:10 pitch:45 heading:0 lookingAtCenterCoordinate:eyeCoordinate];
    NSLog(@"Coordinate %f, %f", coordinate.latitude, coordinate.longitude);
}

- (void)testGetCameraCoordinateForCameraAltitude
{
    CLLocationCoordinate2D eyeCoordinate = CLLocationCoordinate2DMake(37.421424, -122.197812);
    CLLocationCoordinate2D coord = [FSHelper getCameraCoordinateForCameraAltitude:10 pitch:45 heading:0 lookingAtCenterCoordinate:eyeCoordinate];
}

- (void)testGetMapCameraCoordinateForDroneAltitude
{
    CLLocationCoordinate2D droneCoordinates = CLLocationCoordinate2DMake(37.421361, -122.197806);
    CLLocationCoordinate2D mapCoordinate = [FSHelper getMapCameraCoordinateForDroneAltitude:6.9 pitch:45 withBearing:0 andDroneCoordinate:droneCoordinates];
    
}

@end
